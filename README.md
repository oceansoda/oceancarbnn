OceanCarbNN: surface Ocean Carbon Neural Network
================================================

This repository contains the scripts required to run the OceanCarbNN model. 

Usage
-----
```python
import site

site.addsitedir('<path_to_git_repositories>/OceanCarbNN/')
site.addsitedir('<path_to_git_repositories>/all_my_code/')

import all_my_code as amc
import carbnn

carbnn.config.set_config('../path_to_config.yaml')
```

Structure
---------

There are three main modules:

1. **`carbnn.data`**: downloading raw datasets and regridding them to 8-daily 0.25 degrees - this is quite general and can be used for other purposes (e.g., I also use this for download data for total alkalinity predictions)
   - **Downloading** the raw data is done via bash scripts (typically `wget`) that is hosted in their respective directories (i.e., `/net/kryo/work/datasets/...`)
   - **Regridding** is done mostly by `carbnn.data.regrid8D`. 
     - Note that paths to the raw directories are hard-coded in this function. Thus, will break if the raw data is moved.
     - Each data product has it's own function for regridding. Probably not the most generalised, but the easiest to understand and troubleshoot. But below are some more generalised functions
     - `regrid8D.coarsen_and_interp`: takes high resolution data (e.g., 0.01 degrees) and coarsens it to 0.25 degrees, and returns the average and the standard deviation of the coarsening process. It is interpolated exactly to -89.875 : 0.25 : 89.875 and the equivalent for longitude.
     - `DateWindows`: a class that is used to create date windows for the regridding process. Since the starting date of each year is always Jan 1st, the windows are not that straight forward since the last window is shorter and changes based on leap years. Read the documentation for this. 
     - `svd_reconstruction`: A function that fills gappy data (e.g., cloud gaps in Chlorophyll-a) using a simplified version of DINEOF. 
2. **`carbnn.model`**: related to the modelling - includes dataset loading, model building, evaluation, etc.
3. **`carbnn.analysis`**: primarily for calculating the fluxes


Requirements
------------
**Configuration files**:
- See `example/config.yaml` for an example configuration file. The configuration file is used to set the paths to the data and the model.


**Packages**:
- Ipython kernel on Kryo `iacpy3_2023` (has most dependencies installed)
- `tensorflow 2.0` 
- `keras`
- `all_my_code`: https://github.com/lukegre/all_my_code