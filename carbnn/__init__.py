import os 

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from . import utils

# initialize logger and file name configurations 
# as to be done before importing any other modules since these use logger and fname
logger = utils.Logger('CarbNN', format="[%name: %Y-%m-%d %H:%M:%S] %message")

# data + model need to be imported after the logger and fname are initialized
from . import data
from . import model
from . import analysis
from .data.utils import open_dataset

config = model.config
