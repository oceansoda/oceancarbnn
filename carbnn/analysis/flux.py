import xarray as xr
import pandas as pd
import pyseaflux as sf


def calc_sol_and_kw(wind_moment2: xr.DataArray, sst_C: xr.DataArray, salt_psu: xr.DataArray, pres_atm: xr.DataArray) -> xr.Dataset:
    """
    Calculates the solubility and gas transfer velocity of CO2 in seawater.

    Parameters
    ----------
    wind_moment2 : xr.DataArray
        Wind moment squared (m^2/s^2).
    sst_C : xr.DataArray
        Sea surface temperature in Celsius.
    salt_psu : xr.DataArray
        Salinity in practical salinity units (psu).
    pres_atm : xr.DataArray
        Atmospheric pressure in atm.

    Returns
    -------
    xr.Dataset
        Dataset containing the calculated solubility and gas transfer velocity.

    Notes
    -----
    The solubility is calculated using the Weiss (1974) equation, and the gas transfer velocity is calculated using the
    Fay et al. (2021) coefficient and the Schmidt number calculated using the temperature.

    References
    ----------
    Weiss, R. F. (1974). Carbon dioxide in water and seawater: the solubility of a non-ideal gas. Marine Chemistry, 2(3),
    203-215. https://doi.org/10.1016/0304-4203(74)90015-2

    """
    # Calculate solubility using Weiss (1974) equation
    ds = xr.Dataset()
    ds['sol'] = sf.solubility.solubility_weiss1974(
        salt=salt_psu, 
        temp_K=sst_C + 273.15, 
        press_atm=pres_atm, checks=False).assign_attrs(
        units='mol/L/atm',
        description='salinity = climatology of CMEMS-Multiobs; temperature = ESA-CCI v2.0; pressure = ERA5 msl')
    
    # Calculate gas transfer velocity using Fay et al. (2021) coefficient and Schmidt number
    schmidt_number = sf.kw.schmidt_number(sst_C)
    ds['kw'] = (
        0.271 * wind_moment2 * (660 / schmidt_number) ** 0.5
    ).assign_attrs(
        units='cm hr-1',
        description=(
            'wind^2 = ERA5, '
            'temperature = ESA-CCI v2.0, '
            'coefficient_of_gas_transfer = 0.271 (Fay et a. 2021), '
            'schmidt_number = (660 / Sc)**0.5'))
    
    return ds


def compute_flux_aux_vars(t: pd.Timestamp) -> xr.Dataset:
    """
    Compute auxiliary variables for calculating daily air-sea CO2 fluxes.

    Parameters:
    -----------
    t: pd.Timestamp
        The timestamp for which to compute the auxiliary variables.

    Returns:
    --------
    ds: xr.Dataset
        A dataset containing the computed auxiliary variables required to calculate flux,  
        namely, gas transfer velocity, CO2 solubility in seawater, ice, and atmospheric fCO2.
    """
    from .. import config
    
    fname = config.fname  # for convenience and compatibility with v04 and earlier
    data = xr.open_zarr(fname.predictors, group=t.year).sel(time=t, method='nearest', drop=True)

    flux_aux_vars = calc_sol_and_kw(
        wind_moment2 = data.windspeed_moment2,
        sst_C        = data.sst,
        salt_psu     = data.sss,
        pres_atm     = data.press * 9.86923e-6)
    
    # merging data into one file
    ds = xr.merge([data.fco2atm_noaa.rename('fco2atm'), data.ice, flux_aux_vars]).expand_dims(time=[t])
    ds.attrs = dict(
        creation_date=pd.Timestamp.today().strftime('%Y-%m-%d'),
        contact='gregorl@ethz.ch',
        description='computed for calculating daily air-sea CO2 fluxes')
        
    return ds
