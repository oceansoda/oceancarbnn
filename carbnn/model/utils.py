from ..utils import PrettyMunch


class Config(PrettyMunch):
    @property
    def _is_set(self):
        if "version" not in self:
            return False
        return True
    
    def _make_err_msg(self, key):
        if self._is_set:
            msg = f"The key '{key}' is not in the config file"
        else:
            msg = "You need to load the config file with the `set_config` method first"

        return msg

    def __getitem__(self, key):
        if key not in self:
            raise KeyError(key, self._make_err_msg(key))
        return super().__getitem__(key)
    
    def __getattr__(self, key):
        if key not in self:
            raise KeyError(key, self._make_err_msg(key))
        return super().__getitem__(key)
    
    def set_config(self, config_fname):
        config = read_config(config_fname)
        self.update(config)
        print(f"Config file loaded successfully ({config_fname})")


def read_config(config_fname, add_project_path=True):
    from ..utils import PrettyMunch
    from munch import munchify
    import yaml

    # Read the YAML file
    with open(config_fname, 'r') as f:
        infos = yaml.safe_load(f)

    config = replace_dict_placeholders(infos)

    config = munchify(config, factory=PrettyMunch)

    return config


def replace_dict_placeholders(input_dict):
    """
    Parses the provided YAML content, replacing placeholders in file paths
    with corresponding values from the YAML file. If the value is a list,
    it is joined with underscores. This version avoids using list comprehensions
    for better readability.

    :param yaml_content: A string containing the YAML content
    :return: A dictionary with placeholders in file paths replaced
    """
    def process_dict(d, data):
        """
        Recursively processes a dictionary, replacing placeholders in its values.

        :param d: The dictionary to process
        :param data: The dictionary with values to replace the placeholders
        :return: The processed dictionary
        """
        processed = {}
        for k, v in d.items():
            if isinstance(v, (str, list)):
                processed[k] = replace_placeholders(v, data)
            elif isinstance(v, dict):
                processed[k] = process_dict(v, data)
            else:
                processed[k] = v
        return processed
    
    def list_to_concat_str(value):
        return '_'.join(map(str, value))

    def replace_placeholders(value, data):
        """
        Replaces placeholders in a given value (string or list) using the provided data dictionary.

        :param value: The string or list containing placeholders
        :param data: The dictionary with values to replace the placeholders
        :return: The value with placeholders replaced
        """
        if isinstance(value, str):
            values_to_replace = {}
            for k, v in data.items():
                values_to_replace[k] = list_to_concat_str(v) if isinstance(v, list) else v
            return value.format(**values_to_replace)
        elif isinstance(value, list):
            return [replace_placeholders(item, data) for item in value]
        return value

    # Process the dictionary recursively
    output_dict = process_dict(input_dict, input_dict)
    
    return output_dict