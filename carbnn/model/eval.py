import pandas as pd
import numpy as np
import xarray as xr
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score


def rmse(y:pd.Series, yhat:pd.Series=None)-> float:
    """Calculates the root mean squared error"""
    if yhat is None:
        return (y**2).mean()**0.5
    else:
        return mean_squared_error(y, yhat)**0.5


def mae(y:pd.Series, yhat:pd.Series=None)-> float:
    """Calculates the mean absolute error
    
    if yhat is None, then assumed that y is the residual
    """

    if yhat is None:
        return y.abs().mean()
    else:
        return mean_absolute_error(y, yhat)


def bias(y:pd.Series, yhat:pd.Series=None)-> float:
    """Calculates the bias
    
    if yhat is None, then assumed that y is the residual
    """
    if yhat is None:
        return y.mean()
    else:
        return (yhat - y).mean()


def create_masks(y_with_coord_index:pd.Series)-> pd.DataFrame:
    """create Fay&McKinley2014 and coastal masks in the shape of 
    an input series (y)

    Parameters
    ----------
    y_with_coord_index : pd.Series
        the target shape of the mask with an index that contains (lat, lon)

    Returns
    -------
    pd.DataFrame
        two columns coast (bool) and biomes (0-17) where the first
        is from reccap2's definition of the coastal ocean and 
        biomes is from Fay and McKinley (2014)
    """
    from all_my_code import data
    from pandas import DataFrame
    
    coords = y_with_coord_index.index.to_frame()

    reccap2 = data.reccap2_regions.open_ocean
    oceanic = reccap2 > 0
    coastal = data.reccap2_regions.coast.pipe(lambda x: ((~oceanic & ~x) | x))
    fay14 = data.fay_any_mckinley_2014_biomes

    coast = coastal.astype(float).grid.colocate_dataarray(lat=coords.lat, lon=coords.lon, verbose=False).astype(bool)
    biomes = fay14.mean_biomes.astype(float).grid.colocate_dataarray(lat=coords.lat, lon=coords.lon, verbose=False).astype(int)
    reccap2 = reccap2.astype(float).grid.colocate_dataarray(lat=coords.lat, lon=coords.lon, verbose=False)
    biome_names = ['UNSPEC'] + fay14.biome_abbrev.values.tolist()
    
    masks = DataFrame(dict(
        coast=coast,
        biomes=biomes,
        reccap2=reccap2,
    ), index=y_with_coord_index.index)

    masks['reccap2'] = masks.reccap2.fillna(0).astype(int)
    
    return masks


def r2_score_mask(y:pd.Series, yhat:pd.Series, mask:np.ndarray=None)-> float:
    """"Calculates the r2 score for a given mask (see create_masks)"""
    from sklearn.metrics import r2_score as _r2_score
    if mask is not None:
        y = y[mask]
        yhat = yhat[mask]
    r2 = _r2_score(y, yhat)
    return r2


def eval_metrics_regionally(y:pd.Series, yhat:pd.Series, masks:pd.Series)->pd.DataFrame:
    
    results = pd.DataFrame()
    results['Global'] = eval_metrics(y, yhat)
    
    regions = np.sort(masks.unique())
    for key in regions:
        r = masks == key
        yr = y[r]
        yhatr = yhat[r]
        results[key] = eval_metrics(yr, yhatr)
        
    return results
    

def eval_metrics(y:pd.Series, yhat:pd.Series)->pd.DataFrame:
    from sklearn.metrics import r2_score, mean_squared_error
    
    resid = yhat - y
    
    results = pd.Series()
    results['bias_mean'] = resid.mean()
    results['bias_median'] = resid.median()
    results['mean_abs_err'] = resid.abs().mean()
    results['median_abs_err'] = resid.abs().median()
    results['rmse'] = mean_squared_error(y, yhat)**0.5
    results['r2_score'] = r2_score(y, yhat)
    results['count'] = y.size
    results['y_std'] = y.std()
    results['yhat_std'] = yhat.std()
    
    return results


def eval_metrics_all_regions(y, yhat):
    from all_my_code import data
    masks = create_masks(y)
    
    coastal_scores = eval_metrics_regionally(y, yhat, masks.coast)
    coastal_scores = coastal_scores.rename(columns={True: 'Coastal', False: 'Oceanic'}).T

    # now using fay and mckinley
    biome_names = data.fay_any_mckinley_2014_biomes.biome_abbrev.to_series().to_dict()
    biome_names[0] = 'undefined'
    biome_scores = eval_metrics_regionally(y, yhat, masks.biomes)
    biome_scores = biome_scores.rename(columns=biome_names).T

    scores = pd.concat([coastal_scores, biome_scores.drop(['Global', 'undefined'])])
    scores.round(2)
    
    return scores