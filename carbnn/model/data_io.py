"""
This module contains functions for loading and processing the data used in the model.
All the data accessed here has already been preprocessed and is stored in zarr or parquet formats.

The function load_prediction_time_step_as_xarray determines the predictor variables that are used in the model.


"""
from typing import Any
import xarray as xr
import pandas as pd
import numpy as np
import munch as _munch
from ..data.regrid_25km_8daily import DateWindows
from . import config


def load_prediction_time_step_as_xarray(year: int, time_step: int) -> xr.Dataset:
    """Load and process the prediction data for a given year and time step.

    Parameters:
    -----------
    year : int
        The year for which the data is loaded.
    time_step : int
        The time step for which the data is loaded.

    Returns:
    --------
    xr.Dataset
        The processed prediction data as an xarray Dataset.

    Notes
    -----
    - Uses file names from `carbnn.fname` (defined in carbnn.__init__.py)
    - This function defines what the predictor variables are, and is also used when matching up the predictors with the target data.
    
    """

    fname = config.fname  # for compatibility with v04 and earlier
    # current time step
    y1 = year
    t1 = time_step

    # previous time step
    y0 = y1 if t1 != 0 else (y1 - 1)
    t0 = (t1 - 1) if t1 != 0 else 45

    # special case when y1 == 1982 and t1 == 0 (where there is no previous time step)
    if y1 == 1982 and t1 == 0:
        y0 = y1 
        t0 = t1

    # create the time for the current time step to unify all data
    dw = DateWindows()
    time = dw.bin_centers(y1)[t1]

    # open all the data and select the appropriate time step
    dfco2_clim = xr.open_zarr(fname.fco2_clim_prior).dfco2_clim_smooth.rename('dfco2_clim').isel(dayofyear=t1)
    clim = xr.open_zarr(fname.climatology).isel(dayofyear=t1)
    seas = xr.open_zarr(fname.seasonal_diffs).isel(dayofyear=t1)
    pred0 = xr.open_zarr(fname.predictors, group=y0).isel(time=t0)
    pred1 = xr.open_zarr(fname.predictors, group=y1).isel(time=t1)

    # calculate the anomalies (from the seasonal cycle)
    anom = pred1 - clim
    anom['ssh'] = pred1.ssh_anom if 'ssh_anom' in pred1 else 0
    delta = pred1 - pred0

    # rename the variables to include the suffixes
    anoms = anom.rename({k:f"{k}_anom" for k in anom})
    delta = delta.rename({k:f"{k}_delta" for k in delta})
    clims = clim.rename({k:f"{k}_clim" for k in clim})
    seasc = seas.rename({k:f"{k}_seas" for k in seas})

    # drop the 0d coordinates so that data is easily merged
    arr = [pred1[['xco2atm_mauna_loa', 'fco2atm_noaa']], anoms, delta, clims, seasc, dfco2_clim]
    for i, a in enumerate(arr):
        arr[i] = _drop_0d_coords(a)

    # merge all the data and add the time dimension
    ds = (
        xr.merge(arr)
        .expand_dims(time=[time])
        .astype('float32')
        .unify_chunks())

    # if vars from predictor_variables are not in ds, add them with zeros
    for k in config.x_cols:
        if k not in ds:
            ds[k] = np.float32(0.)

    # predictor_variables only
    ds = ds[config.x_cols]

    return ds


def calc_co2thermal(dfco2_clim, sst_deltaT):
    """calculates the thermal component of fCO2 given temperature changes
    using Takahashi et al. (1993)
        fco2_clim * delta_temperature * 0.0423

    Parameters
    ----------
    dfco2_clim : array-like
        climatological estimate of dfco2. Note that a mean value of 367 is added
        to dfco2_clim to convert it (roughly) to fco2_clim. This is sufficient 
        to approximate dfco2therm
    sst_deltaT : array-like
        a change in temperature (eg anomaly, or seasonal amplitude) in degC or K

    Returns
    -------
    array-like
        the thermal contribution of fCO2
    """
    
    fco2_clim = dfco2_clim + 367  # calculated as the average dfco2
    
    fco2_therm = fco2_clim * sst_deltaT * 0.0423

    return fco2_therm


def prep_target_data(year: int, time_step: int) -> tuple:
    """
    Prepare the target data and predictors for a given year and time step.

    Parameters
    ----------
    year : int
        The year for which the data is prepared.
    time_step : int
        The time step for which the data is prepared.

    Returns
    -------
    tuple
        A tuple containing the target and predictors data.

    Notes
    -----
    - The socat data is loaded from the specified year and time step.
    - The thermal contribution to fco2 is calculated using dfco2_clim and sst_anom + sst_seas.
    - The socat and thermal data are added to the dataset.
    - The target data is 'dstar_fco2nonT'.
    """
    fname = config.fname  # for compatibility with v04 and earlier

    socat_sfco2 = xr.open_zarr(fname.socat_gridded, group=year).isel(time=time_step, drop=True)['fco2_socat_mean']
    noaa_xco2mbl = xr.open_zarr(fname.predictors, group=year).isel(time=time_step, drop=True)['xco2mbl_noaa']

    ds = load_prediction_time_step_as_xarray(year, time_step)
    # in versions v03 and before, the sst_seas was added on top of a seasonal dfco2_clim which already contains a seasonal component
    # the updated versions only uses the anomaly to calculate the thermal component
    fco2therm = calc_co2thermal(ds.dfco2_clim, ds.sst_anom)  # + ds.sst_seas)
    
    # instead of calculating dfco2, we calculate dstar_fco2, which subtracts xCO2atm instead of fCO2atm
    ds['dstar_fco2_socat'] = (socat_sfco2 - noaa_xco2mbl).expand_dims(time=ds.time.values)
    ds['dstar_fco2nonT'] = ds.dstar_fco2_socat - fco2therm
    
    df = ds.to_dask_dataframe().dropna().compute().set_index(['time', 'lat', 'lon'])
    
    target = df['dstar_fco2nonT']
    predictors = df.drop(columns=['dstar_fco2nonT', 'dstar_fco2_socat'])
    
    return target, predictors


def time_to_train_test_split_month_index(time, t0=np.datetime64('1982-01'), base=7):
    """
    Calculate the month index for each date based on its relative position to a reference date.

    Parameters
    ----------
    time : list or numpy.ndarray
        List or array of dates (as strings or numpy datetime objects) to compute the indices for.
    t0 : numpy.datetime64, optional
        Reference date. Defaults to January 1982.
    base : int, optional
        Base value for the modulo operation. Defaults to 7.

    Returns
    -------
    numpy.ndarray
        Array of indices for each date in the input list.
    
    Examples
    --------
    >>> time_to_train_test_split_month_index(['1982-02', '1982-08', '1983-01', '1984-02'])
    array([1, 0, 5, 4])
    """

    # Convert the input `time` into a numpy array with a datetime type with monthly resolution.
    t = np.array(time).astype('datetime64[M]')
    
    # Compute the difference in months between each date in `t` and the reference date `t0`.
    # Then, take the modulo of this difference with the base value to get the index.
    index = (t - t0).astype(int) % base
    
    return index


def _drop_0d_coords(da: xr.DataArray) -> xr.DataArray:
    """
    Drops dimensions that are single-dimensional from the input DataArray.

    Parameters:
    -----------
    da : xr.DataArray
        The input DataArray.

    Returns:
    --------
    xr.DataArray
        The resulting DataArray after dropping the single-dimensional coordinates.

    """

    # Remove single-dimensional elements from the DataArray using squeeze()
    da = da.squeeze()

    # Get the coordinates with shape 0 (single-dimensional)
    coords_to_drop = [c for c in da.coords if len(da[c].shape) == 0]

    # Drop the single-dimensional coordinates from the DataArray
    da_dropped_coords = da.drop(coords_to_drop)

    # Return the resulting DataArray after dropping the single-dimensional coordinates
    return da_dropped_coords


def make_train_test_split_index(time, t0=np.datetime64('1982-01'), base=7, time_unit='M'):
    """
    Calculate the month index for each date based on its relative position to a reference date.

    Parameters
    ----------
    time : list or numpy.ndarray
        List or array of dates (as strings or numpy datetime objects) to compute the indices for.
    t0 : numpy.datetime64, optional
        Reference date. Defaults to January 1982.
    base : int, optional
        Base value for the modulo operation. Defaults to 7.
    time_unit : str, optional
        Time unit for the input dates. Defaults to 'M' (monthly). Can also be 'Y' (yearly).

    Returns
    -------
    numpy.ndarray
        Array of indices for each date in the input list.
    
    Examples
    --------
    >>> time_to_train_test_split_month_index(['1982-02', '1982-08', '1983-01', '1984-02'])
    array([1, 0, 5, 4])
    """

    # Convert the input `time` into a numpy array with a datetime type with monthly resolution.
    t = np.array(time).astype(f'datetime64[{time_unit}]')
    
    # Compute the difference in months between each date in `t` and the reference date `t0`.
    # Then, take the modulo of this difference with the base value to get the index.
    index = (t - t0).astype(int) % base
    
    return index


def split_train_test_valid_data(df_x: pd.DataFrame, df_y: pd.DataFrame, test_idx: int = 0) -> _munch.Munch:
    """
    Splits the input data into train, test, and validation subsets based on the given test index.

    Parameters:
    -----------
    df_x : pd.DataFrame
        Input DataFrame containing the feature data.
    df_y : pd.DataFrame
        Input DataFrame containing the target data.
    test_idx : int, optional
        Index indicating which subset will be used for testing (default is 0).
        based on the month of the year but has to be in the range [0, 6].

    Returns:
    --------
    subset : munch.Munch
        A Munch object containing the train, test, and validation subsets of the data.

    Notes:
    ------
    The train, test, and validation subsets are created based on the given test index. 
    The test and validation subsets are based on the month
    """

    # assertion for test_idx 
    assert test_idx in range(7), f'Invalid test_idx: {test_idx}'

    # get the index of each date in the input data
    coords = df_y.index.to_frame()
    idx = make_train_test_split_index(coords.time, time_unit='M')
    valid_idx = (test_idx + 4) % 7

    # create the masks
    mask_test = idx == test_idx
    mask_valid = idx == valid_idx
    mask_train = ~mask_test & ~mask_valid

    # create the subsets with Munch, like a dict but with interactive attributes
    subset = _munch.Munch()
    subset.test = _munch.Munch()
    subset.valid = _munch.Munch()
    subset.train = _munch.Munch()

    # assign the data to each subset
    subset.test.x = df_x[mask_test]
    subset.test.y = df_y[mask_test]
    subset.valid.x = df_x[mask_valid]
    subset.valid.y = df_y[mask_valid]
    subset.train.x = df_x[mask_train]
    subset.train.y = df_y[mask_train]

    return subset


def predictions_to_xarray(yhat: np.ndarray, pred: xr.Dataset, model, yhat_shape=[1, 720, 1440, 5, 7]) -> xr.Dataset:
    """
    Convert model prediction array to an xarray Dataset.

    Parameters
    ----------
    yhat : numpy.ndarray
        Array of predicted values.
    pred : xarray.Dataset
        Original dataset containing time, lat, and lon coordinates.

    Returns
    -------
    xarray.Dataset
        Converted dataset containing predicted values with appropriate dimensions, coordinates, and attributes.
        The following variables are added - 'dfco2nonT_ensemble', 'dfco2nonT_avg', 'dfco2nonT_std'. 


    Notes
    -----
    The 'yhat' array is reshaped to match the desired dimensions of the output xarray.Dataset.
    The resulting dataset includes the 'dstar_fco2nonT_ensemble' variable with dimensions 'time', 'lat', 'lon', 'member', 'subset'.
    Attributes are added to provide descriptions and units for the variable.
    The average and standard deviation along the 'member' and 'subset' dimensions are calculated and added as variables.
    Additional attributes are assigned to 'subset' and 'member' variables.

    """

    # Create an empty dataset
    out = xr.Dataset()

    ens_per_subset = yhat_shape[3]
    subsets = yhat_shape[4]
    total_ensemble_members = ens_per_subset * subsets
    hidden_layers = str(config.hidden_layer_sizes)
    dropout = str(config.dropout_per_layer)
    

    # Convert the array to an xarray DataArray and add it to the dataset
    out['dstar_fco2nonT_ensemble'] = xr.DataArray(
        data=yhat.reshape(*yhat_shape),
        dims=['time', 'lat', 'lon', 'member', 'subset'],
        coords=dict(
            time=pred.time,
            lat=pred.lat,
            lon=pred.lon,
            member=np.arange(yhat_shape[3]),
            subset=np.arange(yhat_shape[4])),
        attrs=dict(
            description=(
                'dstar_fco2nonT from the OceanCarbNN model. '
                f'The model has {total_ensemble_members} ensemble members '
                f'({ens_per_subset} members per test subset, {subsets} test subsets). '
                'The model architecture is a feed-forward neural network with 2 hidden '
                f'layers {hidden_layers} and a dropout rate of {dropout}. The model was trained on the '
                'SOCATv2023 dataset (1982-2022).'),
            units='uatm',
            long_name='$\Delta^* f\mathrm{CO}_2^\mathrm{nonT}$'
        )
    ).chunk({
        'time': yhat_shape[0], 
        'lat': 360, 
        'lon': 360, 
        'member': yhat_shape[3], 
        'subset': 1})

    # Calculate the average and standard deviation along the 'member' and 'subset' dimensions
    out['dstar_fco2nonT_avg'] = out.dstar_fco2nonT_ensemble.mean(['member', 'subset'])
    out['dstar_fco2nonT_std'] = out.dstar_fco2nonT_ensemble.std (['member', 'subset'])

    # Add attributes to the 'subset' and 'member' variables
    out.subset.attrs = dict(description=f'defined as every {yhat_shape[4]}th month starting from 1982-01')
    out.member.attrs = dict(description=f'{yhat_shape[3]} members per subset, all members are identical')

    return out
