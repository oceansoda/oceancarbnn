"""
Functions for building TensorFlow model CarbNN. 

TODO: can build train all the test subset ensemble members together (e.g. 10 x 7) to speed up training. This way, the ensemble can be larger, and the model maybe smaller?

"""
import os
import pandas as pd

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def make_hidden_layers(sizes: list, dropout_rate=0, name="ffnn")->list:
    """
    Create a list of hidden layers for a feedforward neural network.

    Parameters
    ----------
    sizes : list
        List of integers specifying the sizes of the hidden layers.
    dropout_rate : float, optional
        Dropout rate to apply to each hidden layer. Default is 0.
    name : str, optional
        Name prefix for the layers. Default is "ffnn".

    Returns
    -------
    list
        List of hidden layers for the feedforward neural network.
    """

    def make_dense(size, name="ffnn", dropout_rate=0)->list:
        """
        Create a dense hidden layer with batch normalization, dropout, and activation.

        Parameters
        ----------
        size : int
            Size of the dense layer.
        name : str, optional
            Name prefix for the layers. Default is "ffnn".
        dropout_rate : float, optional
            Dropout rate to apply. Default is 0.

        Returns
        -------
        list
            List of layers for the dense hidden layer.
        """
        from keras.layers import BatchNormalization, Dense, Activation, Dropout
        return [
            Dense(size, name=f"{name}_dense_{size}"),
            BatchNormalization(name=f"{name}_norm_{size}"),
            Dropout(dropout_rate),
            Activation('relu', name=f"{name}_activ_{size}")]

    hidden_layers = []

    for size in sizes:
        hidden_layers += make_dense(size, name, dropout_rate=dropout_rate)

    return hidden_layers


def make_ffnn(x, num_y_cols:int=1, hidden_layer_sizes:list=[256], dropout_rate:float=0.15, name:str='model', **compile_kwargs):
    """
    Creates a FFNN using tensorflow with the following layers
    normalisation, (dense, batch_norm, dropout, relu_activation)

    Parameters
    ----------
    x : array-like
        actual training data will also be used to set the normalisation layer
    num_y_cols : int, optional
        number of output columns, by default 1
    hidden_layer_sizes : list, optional
        the size of the hidden layers. Note all hidden layers will have 
        the same structure (dense, batch_norm, dropout, activation), by default [256]
    dropout_rate : float, optional
        the rate of dropout, by default 0.15
    name : str, optional
        just for fun, by default 'model'
    compile_kwargs: key-value
        given to the compiler with defaults 
        loss=mse, metrics=[mse,mae], optimizer=Adam(lr=0.005)

    Returns
    -------
    tf.keras.Sequential
        a compiled model with defaults
    """
    # default settings are for CO2 ffnn
    from keras.layers import Normalization, Input, Dense
    from keras import Sequential
    import tensorflow as tf
    
    n_features_in = x.shape[1]
    normalization = Normalization()
    # if batch size is not set, then defaults to 32 (slow)
    normalization.adapt(x, batch_size=x.shape[0])
    
    layers = []
    layers += Input(shape=(n_features_in), name=f"{name}_input"),
    layers += normalization,
    layers += make_hidden_layers(hidden_layer_sizes, name=name, dropout_rate=dropout_rate)
    layers += Dense(num_y_cols, name=f"{name}_output"),

    model = Sequential(layers, name=name)

    # default properties for compiling
    props = dict(
        loss="mse", 
        metrics=[
            "mean_squared_error", 
            "mean_absolute_error"],
        optimizer=tf.optimizers.Adam(learning_rate=0.005))
    props.update(compile_kwargs)
    model.compile(**props)
    
    return model


def get_model_ensemble(model_file_names: str) -> list:
    """Gets the names for all the models sorted 

    Parameters
    ----------
    model_file_names : str
        the globstring to assess

    Returns
    -------
    list
    """
    from pathlib import Path
    from glob import glob
    import keras
    
    mnames = glob(model_file_names)
    mnames = sorted(mnames)
    models = []
    for name in mnames:
        model = keras.models.load_model(name)
        model._name = Path(name).name.split('.')[0]
        models += model,
        
    return models


def concat_members_into_ensemble_model(models: list, name_prfx='model', inputs=None):
    """
    Takes seperate ensemble members and turns them into a single model
    that is faster to predict!
    """
    import keras
    import tensorflow as tf
    from .. import config

    if inputs is None:
        # get input size from existing model
        shape = models[0].input.shape[1]  
        X = keras.Input(shape=(shape), name=f'{name_prfx}_ens_input')  
    else:
        X = tf.convert_to_tensor(inputs)
    
    # collect outputs of models and concat into single array
    yhat_ensemble = tf.concat([model(X) for model in models], axis=1)
    # create a model from the input/output
    
    ensemble_model = keras.Model(inputs=X, outputs=yhat_ensemble, name=f'{name_prfx}_ensemble')
    
    return ensemble_model


def evaluate(model, x:pd.DataFrame, y:pd.Series)-> pd.Series:
    """wrapper to evaluate model and return a neat pd.Series"""
    from pandas import Series
    scores = Series(
        model.evaluate(x, y, batch_size=y.size, verbose=False),
        index=model.metrics_names)
    # drops loss if the same as another result
    scores = scores.drop_duplicates(keep='last')
    return scores
