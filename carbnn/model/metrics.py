
def summary(y, yhat):
    from numpy import nanmean, isnan, array
    from pandas import Series
    from sklearn.metrics import (
        r2_score, 
        mean_absolute_error, 
        mean_squared_error, 
        mean_absolute_percentage_error)
    
    y = array(y).squeeze()
    yhat = array(yhat).squeeze()
    
    mask = ~isnan(yhat) & ~isnan(y)
    y = y[mask]
    yhat = yhat[mask]
    
    results = Series(dict(
        bias=nanmean(yhat - y),
        mae=mean_absolute_error(y, yhat),
        rmse=mean_squared_error(y, yhat)**0.5,
        mape=mean_absolute_percentage_error(y, yhat),
        r2_score=r2_score(y, yhat),
    ))

    if (y.min() < 0) and (y.max() > 0):
        results = results.drop('mape')
    
    return results