import pandas as pd
import numpy as np

from . import metrics
from .. import logger

X_COLS = [
    'co2_prior',
    'sst_seas', 
    'sst_clim', 
    'chl_clim', 
    'chl_seas', 
    'mld_clim',
    'mld_seas',
    'ssh_clim', 
    'ssh_seas', 
    'sss_clim', 
    'sss_seas',
    'ncoord_a',
    'ncoord_b',
    'ncoord_c',
    'dayofyear_cos',
    'dayofyear_sin']

Y_COLS = 'dfco2_socat'


def estimate_dfco2_prior_for_single_member(flattened_t46y720x1440_with_cols_XY: pd.DataFrame):
    
    train, features, nan_mask = prep_training_data(flattened_t46y720x1440_with_cols_XY)

    # create deep copies so originals are not changed
    train_x = train[X_COLS].copy(deep=True)
    train_y = train[Y_COLS].copy(deep=True)
    features_x = features.loc[nan_mask, X_COLS].copy(deep=True)

    # estimating the prior with the iterative approach
    features_y, scores = estimate_dfco2_prior(train_x, train_y, features_x)

    # putting the predictions back into the full dataframe
    features.loc[nan_mask, 'co2_prior'] = features_y

    # convert flattened data to t46y720x1440 dataarray
    ds = convert_flat_to_dataarray(features['co2_prior'])

    # get scores for the smoothed climatology
    features_y_smooth = ds.dfco2_clim_smooth.to_series().values[train_y.index.values]
    scores += metrics.summary(train_y, features_y_smooth),

    # prepare scores so they can be placed in the dataset
    names = ['extr_1', 'ffnn_2', 'extr_3', 'smooth']
    ds['scores'] = (
        pd.concat(scores, axis=1)
        .set_axis(names, axis=1)
        .stack()
        .to_xarray()
        .rename(level_0='metric', level_1='model')
        .expand_dims('member'))

    return ds


def prep_training_data(df):
    
    train = df[X_COLS + [Y_COLS]].dropna()
    features = df[X_COLS]

    mask = features.notnull().all(axis=1)

    return train, features, mask
    

def build_ffnn(x, **kwargs):
    """Build a feed forward neural network using keras.

    Has the following parameters:
    - hidden_layer_sizes=[32, 16]
    - loss='huber'
    - dropout_rate=0.2

    Parameters
    ----------
    x : pd.DataFrame
        Training data for the predictor variables.
    kwargs : dict
        Additional keyword arguments to pass to the keras.Sequential
    
    Returns
    -------
    object
        The feed forward neural network.
    """
    from . import tensorflow_helpers as tfh    

    layer_sizes = [32, 16]
    logger.info(f"Building FFNN model {layer_sizes}")
    model = tfh.make_ffnn(
        x, 
        hidden_layer_sizes=layer_sizes, 
        loss='huber', 
        dropout_rate=0.2)
    return model


def build_extra_trees(**kwargs):
    """
    Builds an Extra Trees model for regression using the scikit-learn library.

    Args:
        **kwargs: Additional keyword arguments that can be passed to the function.

    Returns:
        model: The built Extra Trees regression model.
    """
    from sklearn.ensemble import ExtraTreesRegressor

    logger.info(f"Building Extra Trees model [min_child_samples=70, estimators=108, boostrapped]")
    model = ExtraTreesRegressor(
        n_estimators=36*3, 
        n_jobs=36, 
        min_samples_leaf=70,
        bootstrap=True,
        verbose=0)
    
    return model


def fit_model_and_predict(model, train_x, train_y, features_x, fit_kwargs={}, predict_kwargs={}):
    
    print("Fitting model: ", model.__class__.__name__)
    model.fit(train_x, train_y, **fit_kwargs)
    
    print("Predicting CO2 prior: ", model.__class__.__name__)
    train_yhat = model.predict(train_x, **predict_kwargs)
    features_yhat = model.predict(features_x, **predict_kwargs)

    scores = metrics.summary(train_y, train_yhat)

    train_x.loc[:, 'co2_prior'] = train_yhat
    features_x.loc[:, 'co2_prior'] = features_yhat

    return scores


def estimate_dfco2_prior(train_x, train_y, features_x):
    
    model_1_extr = build_extra_trees()
    model_2_ffnn = build_ffnn(train_x)
    model_3_extr = build_extra_trees()

    ffnn_kwargs = dict(
        fit_kwargs=dict(batch_size=2048, epochs=15, verbose=0),
        predict_kwargs=dict(batch_size=features_x.shape[0], verbose=0))

    scores = []
    scores += fit_model_and_predict(model_1_extr, train_x, train_y, features_x),
    scores += fit_model_and_predict(model_2_ffnn, train_x, train_y, features_x, **ffnn_kwargs),
    scores += fit_model_and_predict(model_3_extr, train_x, train_y, features_x),

    features_y = features_x['co2_prior']

    return features_y, scores
    

def convert_flat_to_dataarray(yhat: pd.Series):
    import xarray as xr

    # creating empty dataset
    ds = xr.Dataset()

    # convert the flat data to a gridded DataArray
    ds['dfco2_clim'] = xr.DataArray(
        np.array(yhat).reshape(46, 720, 1440), 
        dims=['dayofyear', 'lat', 'lon'],
        coords={
            'dayofyear': np.arange(4, 367, 8), 
            'lat': np.arange(-89.875, 90, 0.25),
            'lon': np.arange(-179.875, 180, 0.25)},
        attrs=dict(
            long_name='fugacity of CO2 climatology',
            units='uatm'))

    # smoothing the estimate to avoid overfitting
    ds['dfco2_clim_smooth'] = smooth_data(
        ds['dfco2_clim'], dayofyear=7, lat=3, lon=3)

    # adding some information
    ds = ds.assign_attrs(
        date=pd.Timestamp.now().strftime('%Y-%m-%d'),
        contact='luke.gregor@sdsc.ethz.ch',
        description=(
            'ensemble members are trained through an iterative approach of FFNN[32, 16], '
            'and extra randomised trees. '
            'The ExtraTrees model has mse loss, ~100 members, min_leaves = 70. '
            'The FFNN always has a dropout of 0.2, and a batch size 2048, learning rate of 0.005 but is only trained for 20 epochs. '
            'The Extra trees is trained first, then the FFNN is trained with the output of the '
            'Extra trees as one of the predictors. The FFNN output is '
            'passed back to Extra trees rounds. The 10 ensemble members are averaged and '
            'then smoothed over time using a 2-month (8 x 7) rolling window. Little spatial '
            'smoothing is applied as spatial gradients should be preserved. '),
        predictors=X_COLS)

    # this is for one member, so we expand the dimension
    ds = ds.expand_dims('member')
    ds = ds.chunk({'member': 1, 'dayofyear': 23, 'lat': 360, 'lon': 360})

    return ds


def smooth_data(da, **kwargs):
    """Rolling mean across a dimension

    Parameters
    ----------
    da : xarray.DataArray
        the data to smooth
    kwargs : dict
        the dimension and window size to smooth across
    """

    def _roll_across_dim(da, **roll_dim):
        roll_dims = list(roll_dim)
        assert len(roll_dims) == 1
        
        dim = roll_dims[0]
        n = da[dim].size
        w = roll_dim[dim]
        wrap_index = np.r_[np.arange(-w, 0), np.arange(n), np.arange(w)]
        
        rolled = (
            da
            .isel(**{dim:wrap_index})
            .rolling(**{dim: w}, center=True, min_periods=1).mean()
            .isel(**{dim: slice(w, -w)}))
        return rolled
    
    rolled = da.copy()
    for key in kwargs:
        rolled = _roll_across_dim(rolled, **{key: kwargs[key]})
    
    return rolled

