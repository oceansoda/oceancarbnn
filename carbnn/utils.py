import pathlib
import logging
import pprint
from munch import Munch, munchify


class PrettyMunch(Munch):
    def __repr__(self):
        return pprint.pformat(self.__dict__)
    

class Logger(logging.Logger):
    """
    A logger that is easier to set up than the default.
    Also provides more consistent behaviour with the
    logging levels as the handle and logger levels are
    set at the startup.
    Basically, syntactic sugar :)
    """

    def __init__(self, name="LOG", level=logging.INFO, format="[%name] %message"):
        """
        Returns a Python logger with more logical logging levels

        Parameters
        ----------
        name : str
            Name of the logger (will appear in front of each message)
        level : int
            logging level as defined in the logging module. Higher is quieter
            10 = debugging,
            20 = info,
            30 = warning,
        format : str
            Format of the messages. The default is "[%(name)s] %(message)s".
            The format can be customized by using the following placeholders:
            %name = name of the logger
            %message = message to log
            %<timeformat> = how to format the time (e.g. %Y-%m-%d OR %H:%M:%S)

        Returns
        -------
        logger : logging.Logger
            The logger object that has been adapted
        """
        import time
        import sys
        import io
        
        logging.Logger.__init__(self, name, level=level)

        handler = logging.StreamHandler(sys.stdout)
        format, date_format = self._get_logger_format(format)
        self.format_ = format
        self.date_format_ = date_format
        formatter = logging.Formatter(fmt=format, datefmt=date_format)
        handler.setFormatter(formatter)
        
        handler.setLevel(self.level)
        self.addHandler(handler)

        self._output = io.StringIO()
        handler = logging.StreamHandler(self._output)
        handler.setFormatter(formatter)
        handler.setLevel(self.level)
        self.addHandler(handler)
        
        self.last_log_time = time.time()
        self.last_log_value = ''
        self.rapid_repetition_counter = 0
        self.rapid_repetition_threshold = 15

        self.setLevel(self.level)

        self.log(15, "Logger initialized with level {}".format(self.level))
    
    def set_level(self, level):
        import sys
        import io
        
        handler = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter(fmt=self.format_, datefmt=self.date_format_)
        handler.setFormatter(formatter)
        
        handler.setLevel(self.level)
        self.addHandler(handler)

        self._output = io.StringIO()
        handler = logging.StreamHandler(self._output)
        handler.setFormatter(formatter)
        handler.setLevel(self.level)
        self.addHandler(handler)

        self.setLevel(self.level)        

    @staticmethod
    def _get_logger_format(format):
        """
        Finds the date in the format string
        """
        import re

        if "%name" in format:
            format = format.replace("%name", "%(name)s")
        if "%message" in format:
            format = format.replace("%message", "%(message)s")

        pattern = "(%[YmdHMS]{1}[\s\./:_-]?(?=[^a-zA-Z]))"  # noqa
        matches = re.findall(pattern, format)
        date_format = "".join(matches)
        if date_format:
            format = format.replace(date_format, "%(asctime)s")
        else:
            date_format = None
        return format, date_format

    def log(self, level, msg, *args, **kwargs):
        """
        Logs a message with the given verbosity level.

        Parameters
        ----------
        level : int
            message loudness. Higher is louder
        """
        import time 

        current_time = time.time()
        same_start = self.last_log_value[:5] == msg[:5]
        recent = (current_time - self.last_log_time) < 0.1
        exceeds_threshold = self.rapid_repetition_counter >= self.rapid_repetition_threshold
        
        if recent and same_start:
            self.rapid_repetition_counter += 1
            if self.rapid_repetition_counter > self.rapid_repetition_threshold:
                return
            elif self.rapid_repetition_counter == self.rapid_repetition_threshold:
                self._log(level, 'repetitive fast-fire messages will not be shown from now on', args, **kwargs)
                return
        elif recent and not same_start:
            self.rapid_repetition_counter = 0
        
        self._log(level, msg, args, **kwargs)
        self.last_log_time = time.time()
        self.last_log_value = msg
        
    def info(self, msg, *args, **kwargs):
        self.log(20, msg, *args, **kwargs)
    
    def warning(self, msg, *args, **kwargs):
        self.log(30, msg, *args, **kwargs) 
            
    def debug(self, msg, *args, **kwargs):
        self.log(10, msg,  *args, **kwargs)

    @property
    def history(self):
        """
        Returns the history of the logger
        """
        return self._output.getvalue()


def run_script(filename:str, *args, verbose:bool=False):
    """
    Runs a bash script with the given filename and arguments.

    Parameters:
        filename (str): The name of the bash script to run.
        args (tuple): The arguments to pass to the bash script.
        verbose (bool): If True, prints the output of the bash script to the console. 
                        Defaults to False.

    Returns:
        CompletedProcess: A subprocess.CompletedProcess object that contains information 
                          about the completed process, including its return code, stdout, 
                          and stderr.
    """
    import subprocess
    
    filename = str(filename)
    
    if filename.endswith(".sh"):
        program = "bash"
    elif filename.endswith(".py"):
        program = "/usr/local/Miniconda3-envs/envs/2023/envs/iacpy3_2023/bin/python"
    else:
        raise ValueError("only supports the following extensions: [.sh, .py]")

    # Convert args to a list of strings and prepend "bash" and the filename to the list
    commands = [program, filename] + [str(a) for a in args]

    # Run the script as a subprocess and capture its output
    result = subprocess.call(
        commands,
        stdout=None if verbose else subprocess.PIPE,
        stderr=None if verbose else subprocess.PIPE)

    # Return the CompletedProcess object
    return result


def prepend_project_path(path:str, resolve=False)->str:
    """returns the full path with the project prefix"""
    
    project_path = get_project_path()

    path = str(path)
    if path.startswith('/'):
        path = path[1:]
    
    full = (project_path / path).absolute()
    if resolve:
        full = full.resolve()
    
    return str(full)


def get_project_path()-> pathlib.Path:
    """returns the project path based on the location of the .env file, 
    if it can't find the .env file, will raise an error telling the 
    user to create the .env file in the project root directory
    """
    import dotenv

    env_path = pathlib.Path(dotenv.find_dotenv())

    if str(env_path) == '.':
        raise FileNotFoundError(
            "Could not find .env file. "
            "Please create .env file in project root directory.")
    
    project_path = env_path.parent

    return project_path
