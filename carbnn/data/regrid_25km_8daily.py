"""
Script to regrid data to 25km and 8daily resolution with the start of each year being the 4th of Jan.

Each variable has a function <var> and each variable <source> has a function.
<var> functions are HIGH LEVEL FUNCTIONS and call the appropriate <source> func, 
    e.g., chlorophyll only from 1998. 
<source> functions are under SOURCE SPECIFIC FUNCTIONS and are called by the <var> functions. 
    File paths are defined in these source-specific functions. 
There are also HELPER FUNCTIONS that are used by the <source> functions.

I've moved away from having a file with all the info since this is not as easy to read and not easy to migrate. 

"""

import sys
import site
import pathlib
import numpy as np
import xarray as xr
import pandas as pd
from functools import lru_cache as _lru_cache
from matplotlib import pyplot as plt

sys.path.insert(0, site.USER_SITE)
import all_my_code
from loguru import logger
from .utils import DateWindows


########################################
## HIGH LEVEL FUNCTIONS
########################################

def process_all(year:int, time_step:int, include_winds=True, fill_chl=True)->xr.Dataset:
    """Process all variables for a given year and time step
    
    Variables are processed in the following order:
    1. sea surface temperature
    2. sea surface salinity
    3. chlorophyll
    4. sea surface height
    5. mixed layer depth
    6. atmospheric co2
    7. winds

    Parameters
    ----------
    year : int
        the year for which you want to process the data
    time_step : int
        the time step of the year for which you want to process the data
    include_winds : bool, optional
        whether to include the winds, by default True
    fill_chl : bool, optional
        whether to fill the chlorophyll data using the SVD approach. 
        set this to true if you are updating a single time step, and 
        set it to false if you are processing an entire year in one go, 
        but then you'll have to run the `make_chl_filled` function separately. 
        note that setting this to true increases processing time per time step 
        to about 2 minutes. by default = True
    
    Returns
    -------
    xr.Dataset : a dataset with all the variables
    """
    today = pd.Timestamp.today()
    most_recent_possible_time_step = today - pd.Timedelta('6D')
    dw = DateWindows()
    bin_edges = dw.bin_edges(year)
    t0, t1 = bin_edges[time_step], bin_edges[time_step + 1] - pd.Timedelta('1S')

    print(f'[REGRIDDING] {year}-{time_step:02d} ({t0:%Y-%m-%d} : {t1:%Y-%m-%d}) raw to 8-daily by 0.25deg')
    
    if t1 > most_recent_possible_time_step:
        print(f"You're caught up! {t1:%Y-%m-%d}, it is too recent")
        return
    
    chl_kwargs = {} if fill_chl else dict(fname_prev=None, fname_clim=None)


    ds = []
    ds += sst(year, time_step),  # sst must be the first variable
    ds += sss(year, time_step),
    ds += chl(year, time_step, **chl_kwargs),
    ds += ssh(year, time_step),
    ds += mld(year, time_step),
    if include_winds:
        ds += winds(year, time_step),
    # co2atm must be one of the last variables in the list (a dimensions thing)
    ds += co2atm(year, time_step),  
    
    ds = xr.merge([da for da in ds if da is not None])
    ds = ds[[key for key in sorted(ds.keys())]]
    ds = ds.chunk({'time': 1, 'lat': 360, 'lon': 360})
    ds['fco2atm_noaa'] = fco2_atmospheric(ds).fco2atm_noaa
    
    ds.attrs = dict(
        summary='predictors that are used by the CARBNN algorithms',
        time_coverage_start=str(t0), 
        time_coverage_end=str(t1),
        time_coverage_duration='P8D',
        geospatial_lat_max=ds.lat.max().values,
        geospatial_lat_min=ds.lat.min().values,
        geospatial_lon_max=ds.lon.max().values,
        geospatial_lon_min=ds.lon.min().values,
    )
    
    return ds


def sst(year, time_step):
    logger.debug("Processing SST (ESA-CCI / NOAA OISST)")
    sst_funcs = sst_cci_daily_to_8daily, sst_noaa_daily_to_8daily
    ds, mask_value = fetch_data_if_available(sst_funcs, year, time_step)

    if ds is None:
        return None

    ds['sst_flag'] = (
        (ds.sst.notnull() * mask_value)
        .astype('int8')
        .assign_attrs(flag_values="1=esa-cci-ostia.v2.1, 2=noaa-oisst.v2"))
    
    return ds
    

def ssh(year, time_step):
    logger.debug("Processing SSH (DUACS")
    ssh_funcs = ssh_duacs_8daily_rep, ssh_duacs_8daily_nrt
    ds = fetch_data_if_available(ssh_funcs, year, time_step)[0]

    return ds


def sss(year, time_step):
    """updated salinity does not have uncertainties"""
    logger.debug("Processing SSS")
    attrs = dict(flag_values="1=esa-cci-v3.21, 2=soda-v3.4.2, 4=cmems-multiobs")
    
    sal, flags = get_salinity_stack(year, time_step)

    ds = xr.Dataset(coords=sal[0].coords)
    ds['sss'] = np.nan
    ds['flags'] = np.nan
    for i in range(len(sal)):
        filler = sal[i].sss
        flag = (filler.notnull().astype(int) * flags[i]).where(lambda x: x > 0)
        ds['sss'] = ds.sss.fillna(filler)
        ds['sss_flag'] = ds.flags.fillna(flag)

    ds['sss_flag'] = ds.sss_flag.fillna(0).assign_attrs(**attrs)
    ds = ds.drop('flags')
    return ds
        
    
def chl(
    year, 
    time_step, 
    fname_prev='/net/kryo/work/gregorl/projects/archive/CARBNN/data/1_gridded/preds_8daily_25km_v01.zarr', 
    fname_clim='/net/kryo/work/gregorl/projects/archive/CARBNN/data/1_gridded/clims_8daily_25km_v01.zarr'):
    """Process chlorophyll for a given year and time 
    step and fill using SVD approach if fname clim given

    If both fname_prev and fname_clim are given, then these files will be 
    used to fill the gaps in the chlorophyll data using the SVD approach.

    Note that this slows down the processing significantly but is more
    robust for years that are not complete (e.g., current year)

    Parameters
    ----------
    year : int
        the year for which you want to process the data
    time_step : int
        the time step of the year for which you want to process the data (on a 8-daily basis)
    fname_prev : str, optional
        path to data from previous time step
    fname_clim : str, optional
        path to climatology data (8-daily)

    Returns
    -------
    xr.Dataset : a dataset with chlorophyll and uncertainties (gridding and from product)
    """    
    logger.debug("Processing CHL (ESA-OC-CCI / GlobColour)")
    chl_funcs = chl_occci_to_8daily, chl_cmems_gapfilled
    ds, mask_value = fetch_data_if_available(chl_funcs, year, time_step)
    if ds is None:
        return None
    
    ds['chl_flag'] = (
        (ds.chl.notnull() * mask_value)
        .assign_attrs(flag_values="1=esa-occci-v6.0, 2=globcolour-cmems-gapfilled")
        .astype('int8'))
    
    if (fname_clim is not None) and (fname_prev is not None):
        chl, chl_clim = prep_chl_filling(ds.chl, fname_prev, fname_clim)
        filled = make_chl_filled(chl, chl_clim).isel(time=[-1])
        ds = xr.merge([ds, filled])

    return ds


def mld(year, time_step):
    logger.debug("Processing MLD (SODA 3.4.2")
    
    if ((year <= 2020) & (time_step <= 44)) | (year < 2020):
        return soda_to_8daily(year, time_step)[['mld']]
    
    if (year == 2020) & (time_step == 45):
        # since all but the last time step of 2020 are present, 
        # raises an error if the last time step is requested
        time = DateWindows().bin_centers(year)[time_step]
        blank_mld = soda_to_8daily(year, 44)[['mld']].assign_coords(time=[time]) * np.NaN
        return blank_mld
    else:
        return xr.Dataset()


def co2atm(year, time_step):
    logger.debug("Processing CO2atm")
    mbl = xco2_noaa_mbl(year, time_step)
    stn = xco2_mauna_loa(year, time_step)
    
    ds = xr.merge([mbl, stn])
    return ds

    
def winds(year, time_step):
    logger.debug("Processing winds (ERA5)")
    return era5_hourly_to_8daily(year, time_step)


########################################
## HELPER FUNCTIONS
########################################
def fetch_data_if_available(funcs, year, time_step):
    for mask_value, func in enumerate(funcs):
        try:
            data = func(year, time_step).astype('float32')
            logger.debug(f"Using function {func.__name__}")
            return data, mask_value + 1
        except OSError:
            continue
    return None, 0


def check_if_safe_to_write(year, time_step, sname):
    """Check if it is safe to write to zarr

    Parameters
    ----------
    year : int
        the year for which you want to process the data
    time_step : int
        the time step of the year for which you want to process the data (on a 8-daily basis)
    sname : str
        path to zarr store

    Returns
    -------
    bool : True if it is safe to write, False if not
    """
    sname = pathlib.Path(sname)

    # get the year and time step from the dataset
    dw = DateWindows()
    time = dw.bin_centers(year)[time_step]
    year = time.year

    # check that time is not within the last 8 days
    today = pd.Timestamp.today()
    most_recent_possible_time_step = today - pd.Timedelta('8D')
    if time > most_recent_possible_time_step:
        raise ValueError(f"Cannot process data for {time:%Y-%m-%d}, it is too recent")

    # make the path to the time chunk (that can be used to check if it exists)
    time_chunk = sname / str(year) / 'time' / str(time_step)
    group = sname /str(year)


    # check if the first time step in the zarr is January 4th
    if time_step == 0 and time_chunk.exists():
        zarr_t0 = xr.open_zarr(sname, group=year).time[0].values.astype('datetime64[D]')
        ds_t0 = time.to_datetime64().astype('datetime64[D]')
        tolerance = np.timedelta64(1, 'D')
        assert abs(zarr_t0 - ds_t0) <= tolerance, f'{zarr_t0} != {ds_t0}'

    # check if the time chunk exists, if so skip
    if time_chunk.exists():
        existing_time_steps = [int(t.name) for t in time_chunk.parent.glob('[0-9]*')]
        max_time_step = max(existing_time_steps)
        raise ValueError(
            f'Time step exists ({year}-{time_step}). '
            f'The latest time step for {year} is {max_time_step} (of 45)'
        )
        
    # check if the previous time chunk exists, if not raise error
    previous_time_chunk = group / "time" / str(time_step - 1)
    # do not raise the error if the time step is 0 and the group does not exist
    if (not previous_time_chunk.exists()) and (time_step != 0):
        raise ValueError(
            f"Trying to write time chunk [{time_step}] to zarr. "
            f"Previous time chunk [{time_step - 1}] does not exist, "
            "please first write previous time chunk to zarr")
    
    return True


def write_to_zarr_safely(ds: xr.Dataset, sname: pathlib.Path):
    """
    Write dataset to zarr directory using years as groups
    Checks if the time step exists in the zarr file before writing

    Parameters
    ----------
    ds : xr.Dataset
        Dataset to write to zarr that contains time dimension
    sname : pathlib.Path
        Path to zarr directory

    Returns
    -------
    None

    Raises
    ------
    ValueError
        If the previous time step does not exist in the zarr file
        to ensure that the time steps are written in order (0 - 46)
    """
    from dask.diagnostics import ProgressBar

    sname = pathlib.Path(sname)

    # get the year and time step from the dataset
    dw = DateWindows()
    time = ds.time.to_index()[0]
    year = ds.time.dt.year.values[0]
    time_step = dw.index_from_date(time)
    group = sname /str(year)

    # check if it is safe to write
    check_if_safe_to_write(year, time_step, sname)

    # write to zarr
    with ProgressBar():
        ds.to_zarr(
            store=sname, 
            mode='a' if group.exists() else None,
            group=year,
            append_dim='time' if group.exists() else None)
        

def interp_lon_gap(da, **new_coords):

    x = da.lon.values
    dx = da.lon.diff("lon").values.mean()
    expanded_lon = np.r_[x[-1], x, x[0]]
    new_lon = np.r_[x[0] - dx, x, x[-1] + dx]

    da_interp = da.sel(lon=expanded_lon).assign_coords(lon=new_lon).interp(**new_coords)

    return da_interp


def calc_uncert(*da_list):
    da = da_list[0]
    summed = da**2
    names = [da.name + "^2"]
    for da in da_list[1:]:
        summed += da**2
        names += da.name + "^2",
    squared = (
        (summed**0.5)
        .assign_attrs(
            description=f"sqrt({' + '.join(names)})"))
    
    return squared


def make_span_dates(year: int, time_step: int)->pd.DatetimeIndex:
    dw = DateWindows()
    dates = dw.span_from_year_index(year, time_step)
    return dates


def get_file_names(fname_fmt:str, year:int, time_step:int)->list:
    from pathlib import Path
    from glob import glob
    
    dates = make_span_dates(year, time_step)
    
    flist = []
    for d in dates:
        fname = fname_fmt.format(t=d)
        if ("?" in fname) or ("*" in fname):
            globbed = glob(fname)
            fname = globbed[0] if len(globbed) > 0 else ''
        if Path(fname).is_file() and (fname not in flist):
            flist += fname,

    flist_sorted = sorted(flist)
    
    return flist_sorted


def coarsen_and_interp(ds, coarse_dict, avg_vars, std_vars, out_time):
    import warnings
    
    warnings.simplefilter("ignore")
    
    coarse_dict.update(boundary='pad')
    
    avg = ds[avg_vars].coarsen(**coarse_dict).mean()
    std = ds[std_vars].coarsen(**coarse_dict).std().rename(**{k: f"{k}_std" for k in std_vars})
    out = xr.merge([avg, std]).interp(
        lat=np.arange(-90+0.125, 90, 0.25), 
        lon=np.arange(-180+0.125, 180, 0.25))
    
    if 'time' in out.coords:
        out.time.attrs = {}
    
    out = (
        out
        .conform()
        .astype('float32')
        .expand_dims(time=[out_time]))
        
    return out


########################################
## SOURCE SPECIFIC FUNCTIONS
########################################        

## SEA SURFACE TEMPERATURE  and  SEA-ICE CONCENTRATION  ##

def sst_cci_daily_to_8daily(year, time_step):

    fnames = dict(
        c3s='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/esacci-c3s_ostia_20cm/C3S-GLO-SST-L4-REP-OBS-SST/{t:%Y}/{t:%Y%m%d}120000-C3S-L4_GHRSST-SSTdepth-OSTIA-GLOB_ICDR2.?-v02.0-fv01.0.nc',
        cci='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/esacci-c3s_ostia_20cm/ESACCI-GLO-SST-L4-REP-OBS-SST/{t:%Y}/{t:%Y%m%d}120000-ESACCI-L4_GHRSST-SSTdepth-OSTIA-GLOB_CDR2.?-v02.0-fv01.0.nc')

    if year < 2017:
        fname = fnames['cci']
    elif year >= 2017:
        fname = fnames['c3s']
    
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    ds = xr.open_mfdataset(flist, parallel=True)
    
    avg_vars = ['analysed_sst', 'sea_ice_fraction', 'analysis_uncertainty']
    std_vars = ['analysed_sst']
    
    if 'CDR2.1' in flist[-1]:
        avg_vars.remove('analysis_uncertainty')
        has_uncert = False
    else:
        has_uncert = True

    coarse = dict(time=8, lat=5, lon=5)
    ds = coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
    ds['analysed_sst'] = (ds.analysed_sst - 273.15).assign_attrs(**ds.analysed_sst.attrs)
    ds['analysed_sst'].attrs.update(source=flist, units='degreesC', valid_min=-2, valid_max=50)
    
    if not has_uncert:
        ds['analysis_uncertainty'] = xr.zeros_like(ds.analysed_sst_std)
    
    ds = (ds
        .rename(
            analysed_sst = 'sst',
            sea_ice_fraction = 'ice',
            analysis_uncertainty = 'sst_sigma_uncert', 
            analysed_sst_std = 'sst_sigma_regrid')
        .assign(
            sst_sigma=lambda ds: calc_uncert(ds.sst_sigma_uncert, ds.sst_sigma_regrid)))

    return ds


def sst_noaa_daily_to_8daily(year, time_step):

    fnames = dict(  # listing paths to raw files
        sst=  '/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/noaa_oisst_20cm/noaa.oisst.v2.highres/sst.day.mean.{t:%Y}.nc',
        sigma='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/noaa_oisst_20cm/noaa.oisst.v2.highres/sst.day.err.{t:%Y}.nc',
        ice='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/noaa_oisst_20cm/noaa.oisst.v2.highres/icec.day.mean.{t:%Y}.nc')

    sst_flist = get_file_names(fnames['sst'], year, time_step)
    sst_sigma_flist = get_file_names(fnames['sigma'], year, time_step)
    ice_flist = get_file_names(fnames['ice'], year, time_step)
    
    flist = sst_flist + sst_sigma_flist + ice_flist
    dates = make_span_dates(year, time_step)
    
    ds = xr.open_mfdataset(flist, parallel=True)
    ds = ds.sel(time=slice(dates[0], dates[-1])).conform()
    avg_vars = ['sst', 'icec', 'err']
    std_vars = ['sst']

    coarse = dict(time=8, lat=1, lon=1)
    ds = (
        coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
        .rename(
            sst = 'sst',
            icec = 'ice',
            err = 'sst_sigma_uncert', 
            sst_std = 'sst_sigma_regrid')
        .assign(sst_sigma=lambda ds: calc_uncert(ds.sst_sigma_uncert, ds.sst_sigma_regrid)))
    
    for key in ds:
        ds[key].assign_attrs(source=flist)

    return ds


## CHLOROPHYLL ##

def chl_occci_to_8daily(year, time_step):

    fname = (
        '/net/kryo/work/datasets/gridded/ocean/2d/observation/chl'
        '/esa_occci/esa_cci_ocean_color_8day_L3S_v6.0/{t:%Y}/'
        'ESACCI-OC-L3S-CHLOR_A-MERGED-8D_DAILY_4km_GEO_PML_OCx-{t:%Y%m%d}-fv6.0.nc')
    
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    ds = xr.open_mfdataset(flist, parallel=True)
    
    ds['chlor_a'] = np.log10(ds.chlor_a).assign_attrs(ds.chlor_a.attrs)
    
    avg_vars = ['chlor_a', 'chlor_a_log10_rmsd']
    std_vars = ['chlor_a']

    coarse = dict(time=1, lat=6, lon=6)
    chl = coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
    ds = (chl
        .rename(
            chlor_a = 'chl',
            chlor_a_log10_rmsd = 'chl_sigma_uncert', 
            chlor_a_std = 'chl_sigma_regrid')
        .assign_attrs(source=flist)
        .assign(chl_sigma=lambda ds: np.log10(calc_uncert(10**(ds.chl_sigma_uncert), 10**(ds.chl_sigma_regrid)))))
    
    for key in ds:
        ds[key].assign_attrs(source=flist)
    
    return ds


def chl_cmems_gapfilled(year, time_step):
    
    fname = (
        '/net/kryo/work/datasets/gridded/ocean/2d/observation/chl'
        '/cmems_globcolour/globcolour_daily_4km_CHLgapfree_REP'
        '/{t:%Y}/{t:%Y%m%d}_cmems_obs-oc_glo_bgc-plankton_myint_l4-gapfree-multi-4km_P1D.nc')
    
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    ds = xr.open_mfdataset(flist, parallel=True).conform()
    ds['CHL'] = np.log10(ds.CHL).assign_attrs(units='log10(mg/m^3)')
    ds['flags_summed']  = (ds.flags == 2).sum('time')

    avg_vars = ['CHL', 'CHL_uncertainty']
    std_vars = ['CHL']

    coarse = dict(time=8, lat=6, lon=6)
    ds = (
        coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
        .rename(
            CHL='chl',
            CHL_uncertainty='chl_sigma_uncert',
            CHL_std='chl_sigma_regrid')
        .assign(chl_sigma_uncert=lambda ds: np.log10(10**(ds.chl) * (ds.chl_sigma_uncert / 100)).assign_attrs(units='log10(mg/m^3)'))
        .assign(chl_sigma=lambda ds: np.log10(calc_uncert(10**(ds.chl_sigma_uncert), 10**(ds.chl_sigma_regrid))))
    )
    
    for key in ds:
        ds[key].assign_attrs(source=flist)
        
    return ds


## SALINITY AND MIXED LAYER DEPTH  ## 

def sss_esacci_to_8daily(year, time_step):
    
    fname = (
        '/net/kryo/work/datasets/gridded/ocean/2d/observation/sss'
        '/esacci_smos/v03.21/7days/{t:%Y}'
        '/ESACCI-SEASURFACESALINITY-L4-SSS-MERGED_OI_7DAY_RUNNINGMEAN_DAILY_25km-{t:%Y%m%d}-fv3.21.nc')
    
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    ds = xr.open_mfdataset(flist, parallel=True)

    avg_vars = ['sss', 'sss_random_error']
    std_vars = ['sss']

    coarse = dict(time=8, lat=1, lon=1)
    ds = (
        coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
        .rename(
            sss = 'sss',
            sss_random_error = 'sss_sigma_uncert', 
            sss_std = 'sss_sigma_regrid')
        .assign(sss_sigma=lambda ds: calc_uncert(ds.sss_sigma_uncert, ds.sss_sigma_regrid))
    )
    
    for key in ds:
        ds[key].assign_attrs(source=flist)

    return ds


def cmems_multiobs_to_8daily(year, time_step):

    if year <= 2021:
        fname = '/net/kryo/work/datasets/gridded/ocean/2d/observation/sss/cmems_multiobs/dataset-sss-ssd-rep-weekly/{t:%Y}/dataset-sss-ssd-rep-weekly_{t:%Y%m%d}T1200Z_P*T0000Z.nc'
    else:
        fname = '/net/kryo/work/datasets/gridded/ocean/2d/observation/sss/cmems_multiobs/dataset-sss-ssd-nrt-weekly/{t:%Y}/dataset-sss-ssd-nrt-weekly_{t:%Y%m%d}T1200Z_P*T0000Z.nc'

    dates = make_span_dates(year, time_step)
    flist = get_file_names(fname, year, time_step)

    # this is a bug that happens on some leap years (e.g. 1996)
    if (flist == []) and (time_step == 45):
        flist0 = get_file_names(fname, year, time_step - 1)
        flist1 = get_file_names(fname, year + 1, 0)
        flist = flist0 + flist1
        
    vars = ['sos', 'sos_error']
    ds = xr.open_mfdataset(flist, parallel=True)[vars]

    ds = (
        ds
        .isel(depth=0, drop=True)
        .mean('time', keep_attrs=True)
        .conform()
        .rename(
            sos='sss',
            sos_error='sss_sigma')
        .assign(sss_sigma_uncert=lambda ds: ds.sss_sigma,)
        .expand_dims(time=[dates[3]]))
    
    for key in ds:
        ds[key].assign_attrs(source=flist)
    
    return ds


def soda_to_8daily(year, time_step):
    
    fname = '/net/kryo/work/updata/soda_clim/soda_3.4.2/5day_orig/soda3.4.2_5dy_ocean_reg_{t:%Y_%m_%d}.nc'

    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    vars = ['salt', 'mlp']
    ds = (
        xr.open_mfdataset(flist, parallel=True)[vars]
        .isel(st_ocean=0, drop=True)
        .rename(xt_ocean='lon', yt_ocean='lat')
        .mean('time')
        .conform()
        .ffill('lon', limit=1)
        .bfill('lon', limit=1)
        .ffill('lat', limit=2)
        .bfill('lat', limit=2)
        .pipe(lambda ds: interp_lon_gap(
            ds, 
            lon=np.arange(-180+.125, 180, .25),
            lat=np.arange(-90.+.125, 90., .25)))
        .assign(mlp=lambda ds: np.log10(ds.mlp + 1).assign_attrs(units='log10(m)'))
        .rename(salt='sss', mlp='mld')
        .expand_dims(time=[dates[3]]))
    
    for key in ds:
        ds[key] = ds[key].assign_attrs(source=flist)
    
    return ds


def get_salinity_stack(year, time_step):
    salinity = []
    flags = []
    try:
        salinity += sss_esacci_to_8daily(year, time_step),
        flags += 1,
    except:
        pass
    try:
        salinity += soda_to_8daily(year, time_step).drop('mld'),
        flags += 2,
    except:
        pass
    try:
        salinity += cmems_multiobs_to_8daily(year, time_step),
        flags += 4,
    except:
        pass

    return salinity, flags


## SEA SURFACE HEIGHT ##
def ssh_duacs_8daily_nrt(year, time_step):
    fname = '/net/kryo/work/datasets/gridded/ocean/2d/observation/ssh/duacs_cmems/dataset-duacs-nrt-global-merged-allsat-phy-l4/{t:%Y}/nrt_global_allsat_phy_l4_{t:%Y%m%d}_????????.nc'
    return ssh_duacs_daily_to_8daily(fname, year, time_step)


def ssh_duacs_8daily_rep(year, time_step):
    fname = '/net/kryo/work/datasets/gridded/ocean/2d/observation/ssh/duacs_cmems/cmems_obs-sl_glo_phy-ssh_my_allsat-l4-duacs-0.25deg_P1D/{t:%Y}/dt_global_allsat_phy_l4_{t:%Y%m%d}_????????.nc'
    return ssh_duacs_daily_to_8daily(fname, year, time_step)


def ssh_duacs_daily_to_8daily(fname, year, time_step):
  
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)

    ds = xr.open_mfdataset(flist, parallel=True).conform()

    avg_vars = ['sla', 'err_sla', 'adt']
    std_vars = ['sla']

    coarse = dict(time=8, lat=1, lon=1)
    ds = (
        coarsen_and_interp(ds, coarse, avg_vars, std_vars, dates[3])
        .rename(
            adt = 'ssh',
            sla = 'ssh_anom',
            err_sla = 'ssh_sigma_uncert', 
            sla_std = 'ssh_sigma_regrid')
        .assign(ssh_sigma=lambda ds: calc_uncert(ds.ssh_sigma_uncert, ds.ssh_sigma_regrid))
        )
    
    for key in ds:
        ds[key].assign_attrs(source=flist)

    return ds


## ATMOSPHERIC CO2 ##
def xco2_mauna_loa(year, time_step):
    
    t = time_step
    dw = DateWindows()
    dates = dw.span_from_year_index(year, t)
    t0, t1 = dw.bin_edges(year)[[t, t+1]]
    if (t1 - t0) < pd.Timedelta('7D'):
        t1 += pd.Timedelta('8D')
    
    if pd.Timestamp.today() < t1:
        return xr.Dataset()
    
    ds = (
        _download_co2_mauna_loa()
        .sel(time=slice(t0, t1))
        .mean('time', keep_attrs=True)
        .astype('float32')
        .expand_dims(time=[dates[3]])
        .assign_attrs(
            units='ppm')
        .to_dataset(name='xco2atm_mauna_loa')
        )
    
    return ds
    
    
@_lru_cache(1)
def _download_co2_mauna_loa():
    
    url = "https://gml.noaa.gov/webdata/ccgg/trends/co2/co2_weekly_mlo.csv"
    
    df = pd.read_csv(
        url, 
        comment='#', 
        parse_dates={'time': ['year', 'month', 'day']}, 
        index_col='time', 
        na_values=-999.99)
    
    da = (
        df
        .rename(columns=lambda s: s.replace(' ', '_'))
        .increase_since_1800
        .rename('xco2atm_mauna_loa')
        .interpolate()
        .to_xarray()
        .assign_attrs(
            source=url,
            units='ppm',
        ))
    
    return da


def xco2_noaa_mbl(year, time_step):
    
    t = time_step
    dw = DateWindows()
    dates = dw.span_from_year_index(year, t)
    t0, t1 = dw.bin_edges(year)[[t, t+1]]
    t1 -= pd.Timedelta('1S')
    
    xco2 = _download_xco2_noaa_mbl()
    latest_year = xco2.time.dt.year[-1].values
    
    if t1.year > latest_year:
        # a simply way of interpolating xCO2 beyond the data
        # is to assume that the difference between the first
        # and last value is constant and then add that difference
        # to the last value for each year beyond the latest year
        xco2_last_year = xco2.sel(time=str(latest_year))
        # creating new time coordinate for prediction year
        r0 = xco2_last_year.time.to_index()[0]
        r1 = pd.Timestamp(year=year, month=r0.month, day=r0.day)
        dt = r1 - r0  # number of days between current year and target year
        n_years = dt.days / 365
        time = xco2_last_year.time.values + dt

        # calculating the difference between first and last value
        xco2_diff = (xco2_last_year[-1] - xco2_last_year[0])
        xco2_diff_mean = xco2_diff.mean() 
        # creating the offset - multiply the mean by the number of years
        # and then add the mean, a way of making sure that the offset doesn't grow 
        # disproportionately between years (northern high lats grow faster)
        diff = xco2_diff_mean * n_years + (xco2_diff - xco2_diff_mean)
        xco2_target_year = (xco2_last_year + diff).assign_coords(time=time)
        xco2 = xr.concat([xco2, xco2_target_year], dim='time')

    ds = (xco2
        .interp(time=dates[3])
        .expand_dims(time=[dates[3]])
        .astype('float32')
        .assign_attrs(units='ppm')
        .to_dataset(name='xco2mbl_noaa')
        )
    
    return ds


@_lru_cache(1)
def _download_xco2_noaa_mbl():
    """Downloads url and reads in the MBL surface file

    Args:
        noaa_mbl_url (str): the address for the noaa surface file
        dest (str): the destination to which the raw file will be saved

    Returns:
        pd.Series: multindexed series of xCO2 with (time, lat) as coords.
    """
    import re
    import pooch
    import numpy as np
    import pandas as pd
    
    # date fetched = "29.11.2023"
    noaa_mbl_url = "https://gml.noaa.gov/ccgg/mbl/tmp/co2_GHGreference.1665799664_surface.txt"

    # save to temporary location with pooch
    fname = pooch.retrieve(url=noaa_mbl_url, known_hash=None)

    # 08.04.2024: updated version of CO2 marine boundary layer from X
    # note that this function cannot read this data
    # fname = "/net/kryo/work/gregorl/projects/OceanSODA-ETHZ/data-in/raw/xco2mbl_noaa/surface.mbl.co2.2023-prerelease.txt"

    # find start line
    is_mbl_surface = False
    for start_line, line in enumerate(open(fname)):
        if re.findall("MBL.*SURFACE", line):
            is_mbl_surface = True
        if not line.startswith("#"):
            break
    if not is_mbl_surface:
        raise Exception(
            "The file at the provided url is not an MBL SURFACE file. "
            "Please check that you have provided the surface url. "
        )

    # read fixed width file CO2
    df = pd.read_fwf(fname, skiprows=start_line, header=None, index_col=0)
    df.index.name = "date"
    # every second line is uncertainty
    df = df.iloc[:, ::2]
    # latitude is given as sin(lat)
    df.columns = np.rad2deg(np.arcsin(np.linspace(-1, 1, 41)))

    # resolve time properly
    year = (df.index.values - (df.index.values % 1)).astype(int)
    day_of_year = ((df.index.values - year) * 365 + 1).astype(int)
    date_strings = ["{}-{:03d}".format(*a) for a in zip(year, day_of_year)]
    date = pd.to_datetime(date_strings, format="%Y-%j")
    df = df.set_index(date)
    df = df.iloc[:-1]  # remove the last value that is for 2020-01-01

    # renaming indexes (have to stack for that)
    df = df.stack()
    index = df.index.set_names(["time", "lat"])
    df = df.set_axis(index)

    da = df.to_xarray().interp(lat=np.arange(-90+.125, 90, .25)).rename('xco2mbl_noaa')
    
    # creating a date at the end of each year so that there are no missing values when 
    # interpolating to 8-daily (otherwise last time step is missing)
    y0 = str(da.time.dt.year.min().values + 1)  # since we subtract a time step, + 1
    y1 = str(da.time.dt.year.max().values + 2)  # need to add 2 to get the last year
    dt = np.timedelta64(1, 'D')
    end_of_year = np.arange(y0, y1, dtype='datetime64[Y]') - dt
    new_dates = np.sort(np.r_[da.time.values, end_of_year])
    # fill the end-of-year time step with a nan, and then interpolate
    da = da.reindex(time=new_dates).interpolate_na('time', limit=1).ffill('time', limit=1)
    
    da.attrs['source'] = noaa_mbl_url
    da.attrs['date_fetched'] = "2023-06-29"
    
    return da


def fco2_atmospheric(ds: xr.Dataset) -> xr.Dataset:
    """
    Calculate atmospheric fCO2 based on a given dataset.

    Parameters:
    -----------
    ds : xr.Dataset
        Input dataset containing necessary variables.

    Returns:
    --------
    xr.Dataset
        Dataset containing the calculated atmospheric fCO2 values.

    Notes:
    ------
    This function calculates atmospheric fCO2 using the provided dataset.
    The input dataset should contain 'sst', 'sss', 'press', and 'xco2mbl_noaa' variables.

    The water vapour pressure is calculated using the Dickson 2007 formulation.
    The resulting pCO2 is converted to fCO2 using the pCO2_to_fCO2 function from the pyseaflux library.
    The resulting fCO2 is assigned appropriate attributes.
    """
    from pyseaflux.vapour_pressure import dickson2007
    from pyseaflux import pCO2_to_fCO2

    pascal_to_atm = 1 / 101325
    salt = ds.sss
    tempC = ds.sst
    tempK = tempC + 273.15
    press_atm = ds.press * pascal_to_atm
    press_vapour = dickson2007(salt, tempK, checks=False)
    press_corrected = press_atm - press_vapour
    
    pco2_atm = ds.xco2mbl_noaa * press_corrected
    fco2_atm = pCO2_to_fCO2(pco2_atm, tempC, ds.press / 100)
    fco2_atm = fco2_atm.assign_attrs(
        units='uatm',
        description=(
            'pressure = ERA5, xco2 = NOAA MBL. '
            'Water vapour pressure from Dickson 2007. '
            'fCO2 calculated using pyseaflux.pCO2_to_fCO2'))
    fco2_atm = fco2_atm.to_dataset(name='fco2atm_noaa')

    return fco2_atm


## WINDS ##

def era5_hourly_to_8daily(year, time_step):
    
    fname = '/net/kryo/work/datasets/gridded/atmosphere/2d/reanalysis/era5/hourly/{t:%Y}/ERA5_{t:%Y_%m_%d}.nc'
    
    flist = get_file_names(fname, year, time_step)
    dates = make_span_dates(year, time_step)
    ds = xr.open_mfdataset(
        flist, 
        parallel=True, 
        # the following params required when expvar also present
        concat_dim='time',
        combine='nested',  
        coords='minimal', 
    )[['msl', 'u10', 'v10']]
    ds = ds.sel(time=slice(dates[0], dates[-1] + pd.Timedelta('23H'))).conform()
    if 'expver' in ds.dims:
        ds = ds.mean('expver')

    ds = (
        ds
        .assign(windspeed_moment2=lambda ds: ds.u10**2 + ds.v10**2)
        .assign(
            windspeed_moment1=lambda ds: ds.windspeed_moment2**0.5,
            msl2=lambda ds: ds.msl**2).drop(['u10', 'v10'])
        .mean('time')
        .conform()
        .interp(
            lat=np.arange(-90+0.125, 90, 0.25), 
            lon=np.arange(-180+0.125, 180, 0.25))
        .expand_dims(time=[dates[3]])
        .assign(
            # calculating the difference of these two gives us the STD
            wind_std=lambda ds: (ds.windspeed_moment2 - ds.windspeed_moment1**2)**0.5,
            pres_std=lambda ds: (ds.msl2 - ds.msl**2)**0.5)
        .rename(msl='press')
        .drop(['msl2'])
        )

    for key in ds:
        ds[key].assign_attrs(source=flist)

    return ds


#########################
## Chlorophyll filling ##
#########################
def svd_reconstruction(arr, first_guess=0, n_components=8, n_iter=10, svd_kwargs=dict(n_iter=2), verbosity=False):
    """
    Fills gappy data using Singular Value Decomposition (SVD) reconstruction.

    Based on the idea that the principle components can be used to iteratively
    reconstruct the data with less uncertainty when applied iteratively. 

    Parameters
    ----------
    arr : np.ndarray [2D]
        The array to be filled structured as (time, stacked space-coords). 
        The missing values should be NaNs and these will be filled. 
    first_guess : np.ndarray [2D] or float
        A first guess of the missing values. If a float is provided, all missing
        values will be filled with this value. If an array is provided, it has to 
        be the same shape as `arr` and will be used as the first guess. An example
        of a good first guess is the climatology. NaNs in the first guess will be
        treated as land values and will not be filled in the final output. 
    n_components : int
        The number of principle components to use in the reconstruction.
    n_iter : int
        The number of iterations to perform.
    svd_kwargs : dict
        Keyword arguments to pass to the `randomized_svd` function from sklearn.
    verbosity : bool
        If True, prints the iteration number and the current error.

    Returns
    -------
    filled : np.ndarray [2D]
        The filled array. If first_guess was provided with NaNs, these values will
        be filled with NaNs in the final output.
    """
    from sklearn.utils.extmath import randomized_svd
    
    def vprint(*args, **kwargs):
        if verbosity >= 2:
            print(*args, **kwargs)
        elif verbosity >= 1:
            print(*args, **kwargs)
    
    vprint(f'SVD reconstruction of Chl-a [{n_iter} iters]')
    X = np.array(arr)
    m = np.isnan(X)
    
    if isinstance(first_guess, (np.ndarray, pd.DataFrame)):
        land_mask = np.isnan(first_guess)
        filler = np.nan_to_num(first_guess, nan=0, neginf=0, posinf=0)[m]
    else:
        land_mask = None
        filler = first_guess
    
    X[m] = filler
    
    error = []
    for i in range(n_iter):
        u,s,v = randomized_svd(X, n_components, **svd_kwargs)
        out = u @ np.diag(s) @ v
        X[m] = out[m]
        error += np.nansum(abs(X[~m] - out[~m])),
        vprint(f"{i:02d}: {error[-1]:.2f}")
        
    
    filled = X
    if land_mask is not None:
        filled[land_mask] = np.NaN
    
    return filled, error


def fill_lat_lon_nearest(da: xr.DataArray)->xr.DataArray:
    chl_nearest = (
        da
        .ffill(dim='time', limit=1)
        .bfill(dim='time', limit=1)
        .ffill(dim='lat', limit=1)
        .bfill(dim='lat', limit=1)
        .ffill(dim='lon', limit=1)
        .bfill(dim='lon', limit=1))
    
    return chl_nearest


def fill_gaps_with_clim_dineof(da:xr.DataArray, clim:xr.DataArray)->xr.DataArray:
    logger.debug("CHL gap filling (SVD approach) may take some time")
    t = da.time.size
    chl_dineof, dineof_err = svd_reconstruction(
        arr=da.values.reshape(t, -1), 
        first_guess=clim.values.reshape(t, -1),
        n_iter=13, verbosity=0)
        
    chl_dineof = xr.DataArray(
        data=chl_dineof.reshape(da.shape),
        coords=da.coords,
        dims=da.dims)
    
    return chl_dineof


def make_chl_filled(chl_year: xr.DataArray, clim: xr.DataArray)->xr.DataArray:
    from dask.diagnostics import ProgressBar

    with ProgressBar():
        chl_nearest = chl_year.pipe(fill_lat_lon_nearest).compute()
        chl_dineof = chl_nearest.pipe(fill_gaps_with_clim_dineof, clim=clim)

    chl_filled = chl_year.fillna(chl_nearest).fillna(chl_dineof)

    mask_year = chl_year.notnull()
    mask_nearest = chl_nearest.notnull()
    mask_dineof = chl_dineof.notnull()

    chl_flag = (
        (mask_year * 1) + 
        (mask_nearest & ~mask_year) * 2 + 
        (mask_dineof & ~mask_year & ~mask_nearest) * 3)
    
    ds = xr.merge([chl_filled.rename('chl_filled'), chl_flag.rename('chl_filled_flag')])
    
    return ds


def prep_chl_filling(chl_current, fname_prev, fname_clim):
    time = chl_current.time.to_index()[0]
    year = time.year

    t1 = time - pd.Timedelta('7D')
    t0 = t1 - pd.Timedelta('357D')

    da_prev = []
    try:
        da_prev += xr.open_zarr(fname_prev, group=year - 1).chl,
    except:
        pass
    try:
        da_prev += xr.open_zarr(fname_prev, group=year).chl,
    except:
        pass

    if da_prev == []:
        raise ValueError(
            "There is no CHL data for the previous time step, "
             "check if the previous time step exists in the file: "
             f"{fname_prev}")

    chl_clim = xr.open_zarr(fname_clim).chl_filled
    chl_prev = xr.concat(da_prev, dim='time')
    
    chl_prev = chl_prev.sel(time=slice(t0, t1))
    chl = xr.concat([chl_prev, chl_current], dim='time')
    chl_clim = chl_clim.sel(dayofyear=chl.time.dt.dayofyear.values)

    return chl, chl_clim