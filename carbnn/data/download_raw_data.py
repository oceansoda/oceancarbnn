from ..utils import prepend_project_path as _prepend_project_path

download_scripts_paths = dict(
    sst_c3s='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/esacci-c3s_ostia_20cm/download_ostia_esacci_c3s.sh',
    sst_noaa='/net/kryo/work/datasets/gridded/ocean/2d/observation/sst/noaa_oisst_20cm/download_sst-avhrr-ncei.sh',
    
    sss_esacci='/net/kryo/work/datasets/gridded/ocean/2d/observation/sss/esacci_smos/download_sss_esacci.sh',
    sss_cmems='/net/kryo/work/datasets/gridded/ocean/2d/observation/sss/cmems_multiobs/download_cmems_multiobs.sh',
    
    chl_occci='/net/kryo/work/datasets/gridded/ocean/2d/observation/chl/esa_occci/download_chl-esa-cci_8day.sh',
    chl_cmems='/net/kryo/work/datasets/gridded/ocean/2d/observation/chl/cmems_globcolour/download_chl-globcolour_daily_4km_CHLgapfree_REP.sh',
    
    ssh_cmems_rep='/net/kryo/work/datasets/gridded/ocean/2d/observation/ssh/duacs_cmems/download_ssh-duacs-cmems-rep.sh',
    ssh_cmems_nrt='/net/kryo/work/datasets/gridded/ocean/2d/observation/ssh/duacs_cmems/download_ssh-duacs-cmems-nrt.sh',
    
    era5='/net/kryo/work/datasets/gridded/atmosphere/2d/reanalysis/era5/hourly/download_era5_hourly.py',
)


def update_data_sources(selected_vars=[]):
    from ..utils import run_script

    if selected_vars == []:
        selected_vars = [k for k in download_scripts_paths]
    else:
        for key in selected_vars:
            if key not in download_scripts_paths:
                raise ValueError(
                    f"Variable {key} not found in download scripts, "
                    f"should be one of:\n {list(download_scripts_paths.keys())}")

    for key in selected_vars:
        script_abspath = download_scripts_paths[key]
        print(f"Running {script_abspath}")
        run_script(script_abspath, verbose=True)


if __name__ == "__main__":
    update_data_sources()
