import pathlib
import numpy as np
import pandas as pd
import xarray as xr
from typing import Optional


class DateWindows:
    """
    A tool to create pd.Datetime windows
    that have a certain frequency span and 
    start on a certain day of the year.
    """
    def __init__(self, start_day_of_year:int=1, window_span:str='8D'):
        """Initiate the object from which
        periods can be generated

        Parameters
        ----------
        start_day : int, optional
            the start day for each year, by default 1
        freq : str, optional
            how large are windows, by default '8D'.
            uses pd.TimeDelta strings
        """
        self.t0 = start_day_of_year
        self.freq = window_span
        self.today = pd.Timestamp.today()
             
    def bin_edges(self, year:int)->pd.DatetimeIndex:
        if isinstance(year, (np.int_, int)):
            dt = pd.Timedelta(self.freq)
            t0 = pd.to_datetime(f"{year}-{self.t0}", format="%Y-%j")
            t1 = pd.Timestamp(f"{year}-12-31") + (dt / 2)
            bin_edges = pd.date_range(t0, t1, freq=self.freq).values
            bin_edges[-1] = pd.Timestamp(f"{year}-12-31 23:59:59")
            return pd.DatetimeIndex(bin_edges)
        elif isinstance(year, (list, np.ndarray)):
            if not (np.diff(year) == 1).all():
                raise ValueError('All year entries must be increasing by 1 year at a time')
            all_bin_edges = []
            for yr in year:
                edges = self.bin_edges(yr)
                if yr != year[-1]:
                    edges = edges[:-1]
                all_bin_edges += edges, 
            all_bin_edges = np.concatenate(all_bin_edges)
            return pd.DatetimeIndex(all_bin_edges)

    def bin_centers(self, year:int)->pd.DatetimeIndex:
        if isinstance(year, (np.int_, int)):
            bin_edges = self.bin_edges(year)
            bin_width = bin_edges[1:].values - bin_edges[:-1].values
            bin_center = (bin_edges[:-1] + bin_width / 2)
            bin_center_vals = bin_center.values
            bin_center_vals[-1] = bin_center[-1].round('1D')
            return pd.DatetimeIndex(bin_center_vals)
        elif isinstance(year, (list, np.ndarray)):
            if not (np.diff(year) == 1).all():
                raise ValueError('All year entries must be increasing by 1 year at a time')
            all_bin_centers = [self.bin_centers(yr) for yr in year]
            all_bin_centers = np.concatenate(all_bin_centers)
            return pd.DatetimeIndex(all_bin_centers)
                
    def span(self, t:pd.Timestamp)->pd.DatetimeIndex:
        """get the period-span for a specific date

        Parameters
        ----------
        t : pandas.Timestamp
            the date for which you want to fetch the 8-day period
        t0 : int, optional
            The start day for the year, by default '1'
        freq: str, optional
            The freqency of the periods
        
        Returns
        -------
        pd.DatetimeIndex : a list of pd.Timestamps for the period. 
        """
        
        # rounding the input time to the nearest day and 
        # then shifting to midday of that day to avoid ambiguous periods
        t = t.floor('D') + pd.Timedelta('12:00:00')
        
        dt = pd.Timedelta(self.freq)
        
        bin_edges = self.bin_edges(t.year)
        bin_center = self.bin_centers(t.year)
        
        idx = abs(bin_center - t).argmin()
        
        period = pd.date_range(bin_edges[idx], bin_edges[idx + 1], inclusive='left')
        
        return period

    def span_from_year_index(self, year:int, idx:int)->pd.DatetimeIndex:
        bin_centers = self.bin_centers(year)
        t = bin_centers[idx]
        return self.span(t)
    
    def index_from_date(self, date:pd.Timestamp)->int:
        
        if isinstance(date, str):
            date = pd.Timestamp(date)

        edges = self.bin_edges(date.year)
        return np.where(date >= edges)[0].max()

    def year_index_from_date(self, date:pd.Timestamp)->tuple:
        year = date.year
        idx = self.index_from_date(date)
        return year, idx
    
    def index_from_dayofyear(self, doy:int, year:int=2001)->int:
        edges = self.bin_edges(year).dayofyear[:-1]
        return np.where(doy >= edges)[0].max()

    def adjacent_year_and_index(self, year:int, idx:int)->list:
        n = self.bin_centers(year).size
        if idx == 0:
            a0 = year - 1, n - 1
            a1 = year, idx + 1
        elif idx >= (n - 1):
            a0 = year, idx - 1
            a1 = year + 1, 0
        else:
            a0 = year, idx - 1
            a1 = year, idx + 1
            
        return a0, a1

    def next_time_step(self, date:pd.Timestamp)->tuple:
        current_year = date.year
        current_idx = self.index_from_date(date)
        next_year, next_idx = self.adjacent_year_and_index(current_year, current_idx)[1]
        next_date = self.bin_centers(next_year)[next_idx]
        return next_date

    def get_most_recent_possible_time_step(self)->pd.Timestamp:
        
        today = pd.Timestamp.today() - pd.Timedelta('5D')
        today_year = today.year
        today_index = self.index_from_date(today)
        
        year, idx = self.adjacent_year_and_index(today_year, today_index)[0]
        date = self.bin_centers(year)[idx]
    
        return date
    
    def get_next_date(self, date)->pd.Timestamp:      
        
        most_recent_possible = self.get_most_recent_possible_time_step()
        
        if date < most_recent_possible:
            next = self.next_time_step(date)
            return next
    
    def get_list_of_next_dates(self, latest_date)->tuple:
        next_time_step = self.get_next_date(latest_date)
        list_of_steps = ()
        while next_time_step is not None:
            list_of_steps += next_time_step,
            next_time_step = self.get_next_date(next_time_step)
    
        return list_of_steps
    
    def get_list_of_next_time_steps_year_idx(self, latest_date)->tuple:
        dates_list = self.get_list_of_next_dates(latest_date)
        next_time_steps = []
        for date in dates_list:
            year = date.year
            idx = self.index_from_date(date)
            next_time_steps += (year, idx),
    
        return next_time_steps


def resolve_wildcard(fname: str) -> Optional[pathlib.Path]:
    """
    Resolve a filename with a wildcard.

    Parameters
    ----------
    fname : str
        The filename to resolve. Can contain a wildcard `*` that will be replaced with any characters.

    Returns
    -------
    Optional[pathlib.Path]
        The resolved filename, or None if no matching file is found.

    Raises
    ------
    ValueError
        If the wildcard in the filename is not specific enough.

    Examples
    --------
    >>> resolve_wildcard("data_*.csv")
    Path('data_2022-01-01.csv')
    """
    fname = pathlib.Path(fname)
    parent = fname.parent
    name = fname.name

    fname_generator = parent.glob(name)
    flist = list(fname_generator)

    if len(flist) == 1:
        return flist[0]
    elif len(flist) > 1:
        raise ValueError(f'wildcard in filename ({name}) is not specific enough')
    else:
        return None


def find_filename_nearest_to_date(fname: str, time: pd.Timestamp, threshold: int = 4) -> Optional[pathlib.Path]:
    """
    Find the filename nearest to a given date.

    Parameters
    ----------
    fname : str
        The filename to search for. Can contain a wildcard `{t}` that will be replaced with the date.
    time : pd.Timestamp
        The date to search for.
    threshold : int, optional
        The number of days to search before and after the given date, by default 4.

    Returns
    -------
    Optional[pathlib.Path]
        The filename nearest to the given date, or None if no matching file is found.

    Examples
    --------
    >>> find_filename_nearest_to_date("data_{t:%Y-%m-%d}.csv", pd.Timestamp("2022-01-01"))
    Path('data_2021-12-31.csv')
    """
    
    # Loop over a range of days before and after the given date
    for d in range(threshold + 1):
        
        # Calculate the time delta for the current day
        dt = pd.Timedelta(days=d)
        
        # Try to resolve the filename for the previous day
        if fname_prev := resolve_wildcard(fname.format(t=time - dt)):
            return pathlib.Path(fname_prev)
        
        # Try to resolve the filename for the next day
        elif fname_next := resolve_wildcard(fname.format(t=time + dt)):
            return pathlib.Path(fname_next)
    
    # Return None if no matching file is found
    return None


def open_dataset(root_path: str, dim='time', variables=None, slicer=slice(0, None))->xr.Dataset:
    """Concatenate all zarr groups in a directory based on the time dimension
    
    Parameters
    ----------
    root_path : str
        The path to the directory containing the zarr groups
    
    Returns
    -------
    xr.Dataset
        The concatenated dataset
    """
    # Get all groups
    root_path = pathlib.Path(root_path)
    groups = sorted([g for g in root_path.iterdir() if g.is_dir()])
    groups = groups[slicer]
    # Open first group
    ds = xr.open_zarr(groups[0])
    # Open the rest and concatenate
    if variables is None:
        all_groups = [xr.open_zarr(group) for group in groups]
    else:
        all_groups = [xr.open_zarr(group)[variables] for group in groups]

    ds = xr.concat(all_groups, dim=dim)
    return ds


def consolidate_output(fname, verbose=False):
    import zarr

    root = zarr.consolidate_metadata(fname)
    groups = list(root)
    
    for group in list(root):
        if verbose: print('.', end='')
        zarr.consolidate_metadata(f"{fname}/{group}")
        zarr.consolidate_metadata(fname, path=group)
    if verbose: print('')