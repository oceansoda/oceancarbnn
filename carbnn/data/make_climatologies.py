import pathlib
import xarray as xr
import numpy as np
from dask.diagnostics import ProgressBar


def calc_8daily_clim(fname_of_regridded_files: str, sname_of_climatologies: str) -> None:
    from .utils import open_zarr_group
    
    fname_of_regridded_files = pathlib.Path(fname_of_regridded_files)
    sname_of_climatologies = pathlib.Path(sname_of_climatologies)

    # years are defined in Gregor, Gruber, Shutler et al. (2024)
    clim8D_years = dict(
        sst = slice('1990', '2019'),
        sss = slice('2000', '2019'),
        ssh = slice('1993', '2019'),
        chl = slice('1998', '2019'),
        mld = slice('1990', '2019'))
    
    # open the regridded dataset
    ds = open_zarr_group(fname_of_regridded_files)

    # for key in ds.data_vars:
    for key in ['chl']:
        print(f'Calculating {key} climatology')
        # select the time period for the climatology
        clim_period = ds[key].sel(time=clim8D_years[key])
        
        # calculating the 8-day climatology
        clim_da = clim_period.groupby('time.dayofyear').mean('time')
        
        # saving climatologies to zarr file
        with ProgressBar():
            clim_da = clim_da.compute()
            clim_ds = clim_da.to_dataset(name=key)
        clim_ds.to_zarr(
            sname_of_climatologies, 
            mode='a' if sname_of_climatologies.exists() else 'w')
        
        if key == 'chl':
            print(f'Calculating {key}_filled climatology')
            clim_ds = calc_filled_chl_clim(clim_da).to_dataset(name=key + '_filled')
            clim_ds.to_zarr(sname_of_climatologies, mode='a')


def calc_8daily_seas(fname_of_climatologies: str, sname_of_seas_clims: str) -> None:

    ds = xr.open_zarr(fname_of_climatologies)
    # calculate the long-term mean
    ds_avg = ds.mean('dayofyear')
    # calculate the seasonal anomalies
    ds_seas_anoms = ds - ds_avg

    ds_seas_anoms.to_zarr(sname_of_seas_clims, mode='w')
    

def calc_filled_chl_clim(chl_clim) -> xr.DataArray:
    """
    Fill chlorophyll in the high-latitude winter regions by filling with
    a scaled long-term mean based on the amount of light available

    The filling is done using a SVD reconstruction that is based on DINEOF

    Parameters
    ----------
    chl_clim : xr.DataArray
        The chlorophyll climatology that is to be filled
    
    Returns
    -------
    chl_clim_scaled : xr.DataArray
        The filled chlorophyll climatology
    """
    from .regrid_25km_8daily import svd_reconstruction

    # now create filled chlorophyll that has been scaled by the light availability
    chl_ltm = chl_clim.mean('dayofyear')

    swr_scaler = make_swr_high_lat_scaler_for_winter_months()
    chl_clim_scaled = np.log10(swr_scaler * 10**chl_ltm).transpose('dayofyear', 'lat', 'lon')

    # prepare data for the SVD reconstruction that is based on DINEOF
    svd_arr = chl_clim.values.reshape(46, -1)
    # the scaled 8-daily climatology is used as the first guess
    svd_first_guess = chl_clim_scaled.values.reshape(46, -1)
    svd_output = svd_reconstruction(svd_arr, svd_first_guess, verbosity=2, n_iter=15)

    chl_clim_scaled = xr.DataArray(
        svd_output[0].reshape(chl_clim.shape),
        dims=['dayofyear', 'lat', 'lon'], 
        coords=chl_clim.coords)
    
    return chl_clim_scaled


def make_swr_high_lat_scaler_for_winter_months()->xr.DataArray:
    """
    Use the SWR to create a scaler for the high latitudes in the 
    winter months for chlorophyll data using GEWEX surface radiation budget data

    GEWEX SRB data is provided in 1x1 degree resolution and daily temporal resolution
    Only data that is light limited is scaled (< 110 W/m^2)

    Parameters
    ----------
    None

    Returns
    -------
    swr_scaler : xr.DataArray
        The scaler for the high latitudes in the winter months
    """
    # filename for GEWEX SRB data - daily climatology for 1983-2017
    fname = (
        "/net/kryo/work/datasets/grd/atm/2d/obs"
        "/surface_radiation_budget/gewex_nasa/Shortwave_daily_local_1"
        "/GEWEXSRB_Rel4-IP_Shortwave_daily_local_1x1_1983-2017_climatology.nc")

    swr_scaler = (
        xr.open_dataset(fname)
        # use the pristine surface downwelling shortwave radiation
        .pri_sw_dn_sfc
        # create 8-daily means
        .coarsen(dayofyear=8, boundary='pad').mean(['time'])  
        # calc zonal mean, interp to 0.25 degree resolution, and fill missing values
        .mean('lon')
        .interp(lat=np.arange(-90 + .125, 90, 0.25))
        .ffill('lat').bfill('lat')
        # clip the data so that 100 W/m^2 is the maximum value values below will be scaled
        .clip(0, 110)
        # scale between 0 and 1
        .pipe(lambda da: (da - da.min()) / (da.max() - da.min()))
        # then clip between 0.1 and 1 so no values are 0
        .clip(0.1, 1)
    )
    
    return swr_scaler


def create_coords(dayofyear: np.ndarray, lat: np.ndarray, lon: np.ndarray)->dict:
    """cos/sin transformations of latitude and longitude

    Parameters
    ----------
    dayofyear : np.ndarray
        day of year
    lat : np.ndarray
        latitude in degrees N
    lon : np.ndarray
        longitude in degrees E

    Returns
    -------
    dict
        contains keys 
            dayofyear_cos, dayofyear_sin, 
            ncoord_a, ncoord_b, ncoord_c
    """
    coords = dict(
        dayofyear_cos=np.cos((2 * np.pi * dayofyear) / 366),
        dayofyear_sin=np.sin((2 * np.pi * dayofyear) / 366),
        ncoord_a=np.sin(np.deg2rad(lat)),
        ncoord_b=np.sin(np.deg2rad(lon)) * np.cos(np.deg2rad(lat)),
        ncoord_c=np.cos(np.deg2rad(lon)) * np.cos(np.deg2rad(lat)))
    
    # convert all arrays in coords to float32
    for key in coords:
        coords[key] = coords[key].astype(np.float32)

    return coords
