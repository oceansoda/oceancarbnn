import os
import numpy as np
import xarray as xr
import pandas as pd

from .. utils import prepend_project_path
from .. import logger


def main(sname_zarr:str):
    fname_tsv, fname_pq = socat_tsv_to_parquet(
        dest_dir=prepend_project_path('data/0_raw/socat/v2023'))
    
    df = pd.read_parquet(fname_pq)

    for year in range(1982, 2023):
        logger.info(f'Gridding SOCATv2023: {year}')
        
        like = create_dummy_array(year)
        ds = grid_socat_ship_tracks(df, like)
        
        compress = {k: dict(dtype='float32') for k in ds.data_vars}
        ds.to_zarr(
            sname_zarr, 
            mode='a', 
            group=year, 
            consolidated=True,
            encoding=compress)


def socat_tsv_to_parquet(
        dest_dir:str, 
        url="https://www.ncei.noaa.gov/data/oceans/ncei/ocads/data/0278913/SOCATv2023.tsv", 
        **kwargs):
    from all_my_code.munging.name_matching import guess_coords_from_column_names
    from all_my_code import download_file
    import polars as pl
    
    fname = download_file(
        url,
        path=dest_dir,
        verbosity=3)
    
    name = url.split('/')[-1].split('.')[0]
    sname = os.path.join(dest_dir, f"{name}.pq")
    if os.path.isfile(sname):
        logger.info(f'Returning existing file: {sname}')
        return fname, sname
    else:
        logger.info(f"Processing raw SOCAT data")
    
    logger.info(f'Reading file  with polars: {fname}')
    starting_line = get_line_num_match_start_end(
        fname, 
        line_start='Expocode', 
        line_end='fCO2rec_flag') 
    df = pl.read_csv(
        fname, 
        skip_rows=starting_line - 2, 
        skip_rows_after_header=2, 
        sep='\t', 
        columns=[
            'Expocode',
            'yr', 'mon', 'day', 'hh', 'mm', 'ss',
            'longitude [dec.deg.E]', 'latitude [dec.deg.N]',
            'sal', 'WOA_SSS',
            'SST [deg.C]',
            'PPPP [hPa]', 'NCEP_SLP [hPa]',
            'dist_to_land [km]',
            'fCO2rec [uatm]', 'fCO2rec_flag'],
        dtypes={"yr": str, "mon": str, "day": str, 
                "hh": str, "mm": str, "ss": str, 
                "Expocode": str, 'dist_to_land [km]': float}, 
        infer_schema_length=1000)

    df = df.rename({s: _make_col_name_compat(s) for s in df.columns})
    
    logger.info(f'Converting date and time strings to datetime64')
    date = (
        df[['yr', 'mon', 'day', 'hh', 'mm', 'ss']]
        .sum(axis=1)
        .str.strptime(pl.Datetime, fmt='%Y%m%d%H%M%S.', strict=False, exact=False))
    df = df.with_columns([date.alias("date_time")])
    df = df.drop(['yr', 'mon', 'day', 'hh', 'mm', 'ss'])
    df = df.rename(guess_coords_from_column_names(df.columns))
    
    logger.info(f'Saving data to parquet: {sname}')
    df.write_parquet(sname)
    
    return fname, sname


def get_line_num_match_start_end(fname, line_start='Expocode', line_end='fCO2rec_flag'):
    line_end = line_end + '\n'
    with open(fname, encoding='latin') as tsv:
        start_line = None
        for n, line in enumerate(tsv):
            if start_line:
                break
            if line.startswith(line_start) & line.endswith(line_end):
                start_line = n
    return start_line


def _make_col_name_compat(s):
    import re
    ptn = re.compile('\W+')
    s = ptn.sub('_', s)
    s = s.replace('__', '_')
    s = s[:-1] if s.endswith('_') else s
    s = s.lower()
    return s


def grid_socat_ship_tracks(socat_df, like_arr: xr.DataArray):
    """
    Grid the SOCAT ship tracks to the same grid as the target data

    Parameters
    ----------
    socat_df : pd.DataFrame 
        The SOCAT data as a DataFrame that has been read from the 
        SOCAT TSV file
    like_arr : xr.DataArray
        The target data that the SOCAT data will be gridded to
        Should be a single year since the full SOCAT data is too 
        large to grid to a single array

    Returns
    -------
    ds : xr.Dataset
        The gridded SOCAT data containing the following variables
            - fco2_socat_mean: the mean fCO2 from the SOCAT data
            - fco2_socat_std: the standard deviation of fCO2 from the SOCAT data
            - sst_socat_mean: the mean SST from the SOCAT data
            - sst_socat_std: the standard deviation of SST from the SOCAT data
            - sss_socat_mean: the mean SSS from the SOCAT data
            - sss_socat_std: the standard deviation of SSS from the SOCAT data
    """
    assert isinstance(like_arr, (xr.DataArray)), "'like_arr' must be a DataArray or Dataset"
    
    keep = dict(
        time='time', 
        lat='lat',
        lon='lon',
        fco2rec_uatm='fco2_socat',
        sst_deg_c='sst_socat',
        sal='sss_socat')
    
    logger.debug(
        'amc.munging.gridding.grid_to_target_array() assumes '
        'that the time grid is left labelled (not center as with lat,lon)')

    socat_df = socat_df[list(keep)].rename(columns=keep)

    # grid the SOCAT data to the same grid as the target data
    # using all_my_code.munging.gridding
    ds = socat_df.gridding.grid_to_target_array(
        like_arr, 
        aggregators=('mean', 'std'), 
        # sparse=False, 
        verbosity=10)

    return ds


def create_dummy_array(year):
    like = xr.DataArray(
        dims=('time', 'lat', 'lon'),
        coords={
            'time': pd.date_range(f'{year}-01-01', f'{year}-12-31', freq='8D'),
            'lat': np.arange(-90 + 0.125, 90, 0.25),
            'lon': np.arange(-180 + 0.125, 180, 0.25),
        })
    
    return like